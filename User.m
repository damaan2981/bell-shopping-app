//
//  User.m
//  App
//
//  Created by Internet Wu on 03/09/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

#import "User.h"

@implementation User

+ (NSString *)dynamoDBTableName {
    return @"Users";
}

+ (NSString *)hashKeyAttribute {
    return @"ID";
}

@end

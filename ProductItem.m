//
//  ProductItem.m
//  App
//
//  Created by Internet Wu on 24/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import "ProductItem.h"

@implementation ProductItem
@synthesize product_id;
@synthesize product_brand_id;
@synthesize product_name;
@synthesize product_description;
@synthesize product_available_quantity;
@synthesize picture;
@synthesize price;
@synthesize hasImage = _hasImage;
@synthesize failed = _failed;
@synthesize URL = _URL;


- (id) init
{
    self = [super init];
   
    return self;
}

- (void)setProduct_id:(int)id{
    product_id = id;
}

- (void)setProduct_brand_id:(int)brand_id{
    product_brand_id = brand_id;
}

- (void)setProduct_name:(NSString *)name{
    product_name = [[NSString alloc] initWithString:name];
}

- (void)setProduct_description:(NSString *)description{
    product_description = [[NSString alloc] initWithString:description];
}

- (void)setProduct_available_quantity:(int)quantity{
    product_available_quantity = quantity;
}

- (void)setPicture:(UIImage *)photo{
    picture = photo;
}

- (void)setProductURL:(NSURL *)url{
    _URL = url;
}

- (void)setPrice:(float)markup{
    price = markup;
}

- (int)getProduct_id {
    return product_id;
}

- (int)getProduct_brand_id {
    return product_brand_id;
}

- (NSString *)getProduct_name {
    return product_name;
}

- (NSString *)getProduct_description {
    return product_description;
}

- (int)getProduct_available_quantity {
    return product_available_quantity;
}

- (UIImage *)getPicture {
    return picture;
}

- (float)getPrice {
    return price;
}

- (NSURL *)getProductURL {
    return _URL;
}



- (BOOL)hasImage {
    return picture != nil;
}


- (BOOL)isFailed {
    return _failed;
}

@end

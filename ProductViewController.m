//
//  ProductViewController.m
//  App
//
//  Created by Internet Wu on 25/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import "ProductViewController.h"
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

@interface ProductViewController ()

@end

@implementation ProductViewController
@synthesize item;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createSwipes];
    [self.view setBackgroundColor: [UIColor whiteColor]];
    //TODO: Replace this with an Add To Card Button and put it instead into CardViewController.h
    if([PKPaymentAuthorizationViewController canMakePaymentsUsingNetworks:@[PKPaymentNetworkAmex, PKPaymentNetworkMasterCard, PKPaymentNetworkVisa] capabilities:PKMerchantCapabilityCredit]){
      PKPaymentButton *applePayButton = [[PKPaymentButton alloc] initWithPaymentButtonType:PKPaymentButtonTypeBuy paymentButtonStyle:PKPaymentButtonStyleBlack];
      [applePayButton setFrame: CGRectMake(0,0,100,100)];
      [applePayButton addTarget:self
                      action:@selector(clicked:)
                      forControlEvents:UIControlEventTouchUpInside];
       applePayButton.tag = 1;
    
       [self.view addSubview:applePayButton];
    }
    
}

-(void)clicked:(UIButton*)sender{
    
    //open the Stripe payment form
    if(sender.tag==1){
        PKPaymentRequest *request = [Stripe
                                     paymentRequestWithMerchantIdentifier:@"merchant.com.bellplatform"];
        [request setCurrencyCode:@"USD"];
        [request setCountryCode:@"US"];
        [request setMerchantCapabilities: PKMerchantCapabilityCredit];
        [request setSupportedNetworks: @[PKPaymentNetworkAmex, PKPaymentNetworkMasterCard, PKPaymentNetworkVisa]];
        
        // Will later be loaded dynamically from data source.
        NSString *label = [item getProduct_name];
        NSDecimalNumber *amount = [NSDecimalNumber decimalNumberWithString:@"10.00"];
        request.paymentSummaryItems = @[
                                        [PKPaymentSummaryItem summaryItemWithLabel:label
                                                                            amount:amount]
                                        ];
        
        if ([Stripe canSubmitPaymentRequest:request]) {
            PKPaymentAuthorizationViewController *paymentController = [[PKPaymentAuthorizationViewController alloc]
                                                   initWithPaymentRequest:request];
            paymentController.delegate = self;
            [self presentViewController:paymentController animated:YES completion:nil];
        }
        else {
            // Show the user your own credit card form (see options 2 or 3)
        }
    }
}

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
                       didAuthorizePayment:(PKPayment *)payment
                                completion:(void (^)(PKPaymentAuthorizationStatus))completion {
    //something like the below to get token
   /* [Stripe createTokenWithPayment:payment
                        completion:^(STPToken *token, NSError *error) {
                            // charge your Stripe token as normal
                        }];*/

    
    NSLog(@"Payment was authorized: %@", payment);
    
    // do an async call to the server to complete the payment.
    // See PKPayment class reference for object parameters that can be passed
    BOOL asyncSuccessful = FALSE;
    
    // When the async call is done, send the callback.
    // Available cases are:
    //    PKPaymentAuthorizationStatusSuccess, // Merchant auth'd (or expects to auth) the transaction successfully.
    //    PKPaymentAuthorizationStatusFailure, // Merchant failed to auth the transaction.
    //
    //    PKPaymentAuthorizationStatusInvalidBillingPostalAddress,  // Merchant refuses service to this billing address.
    //    PKPaymentAuthorizationStatusInvalidShippingPostalAddress, // Merchant refuses service to this shipping address.
    //    PKPaymentAuthorizationStatusInvalidShippingContact        // Supplied contact information is insufficient.
    
    if(asyncSuccessful) {
        completion(PKPaymentAuthorizationStatusSuccess);
        
        // do something to let the user know the status
        
        NSLog(@"Payment was successful");
        
        //        [Crittercism endTransaction:@"checkout"];
        
    } else {
        completion(PKPaymentAuthorizationStatusFailure);
        
        // do something to let the user know the status
        
        NSLog(@"Payment was unsuccessful");
        
        //        [Crittercism failTransaction:@"checkout"];
    }
}

- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"Finishing payment view controller");
    
    // hide the payment window
    [controller dismissViewControllerAnimated:TRUE completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createSwipes{
    UISwipeGestureRecognizer *swipeRightGesture=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    [self.view addGestureRecognizer:swipeRightGesture];
    swipeRightGesture.direction=UISwipeGestureRecognizerDirectionRight;
}




@end
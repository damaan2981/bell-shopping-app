//
//  PendingOperations.h
//  App
//
//  Created by Internet Wu on 13/11/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

//Custom SubClass of NSOperation
//Keep track of download operations in the ListCollectionViewController class

#import <Foundation/Foundation.h>

@interface PendingOperations : NSObject

//Keep track of active and pending downloads
@property (nonatomic, strong) NSMutableDictionary *downloadsInProgress;
@property (nonatomic, strong) NSOperationQueue *downloadQueue;

@end

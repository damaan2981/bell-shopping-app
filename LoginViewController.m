
//
//  LoginViewController.m
//  App
//
//  Created by Internet Wu on 03/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

//TODO: It is important that when unauthenticated guests come back after a few weeks, they still have the same ID

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "DeveloperAuthenticatedIdentityProvider.h"
#import "LoginViewController.h"
#import "FBSDKLoginKit/FBSDKLoginKit.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *eText;
@property (weak, nonatomic) IBOutlet UITextField *pText;
@property (weak, nonatomic) IBOutlet UIButton *logButton;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *loginButton;

@end



@implementation LoginViewController
@synthesize eText;
@synthesize pText;
@synthesize logButton;
@synthesize loginButton;
@synthesize FBID;
@synthesize progressIndicator;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //check whether the user is logged in via Facebook
    if ([FBSDKAccessToken currentAccessToken]) {
        FBID = [FBSDKAccessToken currentAccessToken].userID;

    }
    
    [loginButton setDelegate:self];
    loginButton.readPermissions =  @[@"email"];

    
    // Do any additional setup after loading the view.
    self.eText.delegate = self;
    self.pText.delegate = self;
    logButton.tag = 1;
    [logButton addTarget:self
                  action:@selector(clicked:)
        forControlEvents:UIControlEventTouchUpInside];
    
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)clicked:(UIButton*)sender{

    
     if(sender.tag == 1){
         if ([eText.text length] == 0 || [pText.text length] == 0) {
             UIAlertController * alert=   [UIAlertController
                                           alertControllerWithTitle:@"Oops!"
                                           message:@"Make sure you enter an email and password!"
                                           preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* ok = [UIAlertAction
                                  actionWithTitle:@"OK"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                      
                                  }];
             [alert addAction:ok];
             [self presentViewController:alert animated:YES completion:nil];
         }
         else{
         AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
             AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
             queryExpression.indexName = @"Email-index";
             queryExpression.hashKeyAttribute = @"Email";
             queryExpression.hashKeyValues = eText.text;
             
             [[dynamoDBObjectMapper query:[User class]
                               expression:queryExpression]
              continueWithBlock:^id(AWSTask *task) {
                  
                  if (task.error) {
                      NSLog(@"The request failed. Error: [%@]", task.error);
                  }
                  if (task.exception) {
                      NSLog(@"The request failed. Exception: [%@]", task.exception);
                  }
                  if (task.result) {
                      
                      AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
                      if([paginatedOutput.items count]==0){
                          NSLog(@"Login Failed");
                      }
                      else{
                          for(User *user in paginatedOutput.items){
                              if([user.Password isEqualToString:pText.text]){
                                  user.Login = true;
                                  [[dynamoDBObjectMapper save:user]
                                   continueWithBlock:^id(AWSTask *task) {
                                       if (task.error) {
                                           NSLog(@"The request failed. Error: [%@]", task.error);
                                       }
                                       if (task.exception) {
                                           NSLog(@"The request failed. Exception: [%@]", task.exception);
                                       }
                                       if (task.result) {
                                           //Do something with the result.
                                       }
                                       return nil;
                                   }];
                                  break;
                              }
                          }
               
                
            
                        
                           
                          if([paginatedOutput.items count]!=0){
                          UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          ControlPanelNavigation *controlVC = [storyboard instantiateViewControllerWithIdentifier:@"controlPanelNavigator"];
                          controlVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                          ControlPanelViewController *controlPanelVC = (ControlPanelViewController *)controlVC.topViewController;
                          if(progressIndicator!=nil){
                              
                              controlPanelVC.progressIndicator = progressIndicator;
                              
                          }
                        
                          controlPanelVC.FBlogin.hidden = YES;
                          controlPanelVC.loginText = @"Logged In";
                              
                              
                          AWSCognitoCredentialsProvider *credentialsProvider =                                            [[AWSCognitoCredentialsProvider alloc]
                                                                        initWithRegionType:AWSRegionUSEast1
                                                                        identityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
                         
                              AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
                              
                              [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
                          [credentialsProvider clearKeychain];
                        
                              
                              AWSCognitoCredentialsProvider *credentialsProvider1 =                                            [[AWSCognitoCredentialsProvider alloc]
                                                                                                                               initWithRegionType:AWSRegionUSEast1
                                                                                                                               identityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
                              
                              AWSServiceConfiguration *configuration1 = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider1];
                              
                              [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration1;
                              
                              
                              AWSCognitoIdentity *identityClient = [AWSCognitoIdentity defaultCognitoIdentity];
                              AWSCognitoIdentityGetOpenIdTokenForDeveloperIdentityInput *idRequest = [[AWSCognitoIdentityGetOpenIdTokenForDeveloperIdentityInput alloc] init];
                              [idRequest setIdentityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
                              [idRequest setTokenDuration:@(60 * 5)];
                              [idRequest setIdentityId:credentialsProvider1.identityId];
                              //[idRequest setLogins:@{@"login.bellplatform.app": credentialsProvider.sessionKey}];
                              [idRequest setLogins:@{@"login.bellplatform.app": eText.text}];
                          
                               dispatch_semaphore_t sema = dispatch_semaphore_create(0);
                              [[identityClient getOpenIdTokenForDeveloperIdentity:idRequest] continueWithBlock:^id(AWSTask *task) {
                                  
                                  if (task.error) {
                                      NSLog(@"Error: %@", task.error);
                                  }
                                  else {
                                      
                                      //in responseID are stored the cached identityID and the token
                                      //responseId.token
                                      //responseId.identityId
                                      AWSCognitoIdentityGetOpenIdTokenForDeveloperIdentityResponse *responseId = task.result;
                                      NSLog(@"%@",responseId);
                    [credentialsProvider1 setLogins:@{@"cognito-identity.amazonaws.com": responseId.identityId}];
                                      AWSCognitoIdentityGetCredentialsForIdentityInput *credInput = [[AWSCognitoIdentityGetCredentialsForIdentityInput alloc]init];
                                      [credInput setIdentityId: responseId.identityId];
                                      [credInput setLogins: credentialsProvider1.logins];
                                      [[identityClient getCredentialsForIdentity:credInput] continueWithBlock:^id(AWSTask *task) {
                                          if (task.error) {
                                              NSLog(@"Error: %@", task.error);
                                          }
                                          else if(task.result){
                                              AWSCognitoIdentityGetCredentialsForIdentityResponse *credResult = [[AWSCognitoIdentityGetCredentialsForIdentityResponse alloc]init];
                                         
                                              
                                          }
                                          dispatch_semaphore_signal(sema);
                                          return nil;
                                      }];
                                      
                                  }
                                  return nil;
                              }];
                       while (dispatch_semaphore_wait(sema, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]]; }
                            
                              
                              [[credentialsProvider1 refresh] continueWithBlock:^id(AWSTask *task) {
                                  return nil;
                              }];
                     
                              
                              dispatch_async(dispatch_get_main_queue(), ^(void){
                          [self presentViewController:controlVC animated:YES completion:nil];
                              });
                          }
                          
                      }
                  }
                  return nil;
              }];
         }
      }
}

- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error{
   
    if ([FBSDKAccessToken currentAccessToken]) {
        FBID = [FBSDKAccessToken currentAccessToken].userID;
        NSString *token = [FBSDKAccessToken currentAccessToken].tokenString;
        AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                                                              initWithRegionType:AWSRegionUSEast1
                                                              identityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
        credentialsProvider.logins = @{ @(AWSCognitoLoginProviderKeyFacebook): token };
       [self handleLoginWithSignInProvider:AWSSignInProviderTypeFacebook];
        
    }
}

//Used for Facebook Login
- (void)handleLoginWithSignInProvider:(AWSSignInProviderType)signInProviderType{
    
   
    [[AWSIdentityManager sharedInstance] loginWithSignInProvider:signInProviderType
                                               completionHandler:^(id result, NSError *error) {
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
                AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
                
                //This FBID is henceforth used as primary identification
                queryExpression.indexName = @"FBID-index";
                queryExpression.hashKeyAttribute = @"FBID";
                queryExpression.hashKeyValues = [FBSDKAccessToken currentAccessToken].userID;
                                                        
                [[dynamoDBObjectMapper query:[User class]
                                       expression:queryExpression]
                                       continueWithBlock:^id(AWSTask *task) {
                                                                
                                        if (task.error) {
                                            NSLog(@"The request failed. Error: [%@]", task.error);
                                        }
                                        if (task.exception) {
                                            NSLog(@"The request failed. Exception: [%@]", task.exception);
                                        }
                                        if (task.result) {
                                            
                                            
                                            AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                                                                                                  initWithRegionType:AWSRegionUSEast1
                                                                                                  identityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
                                            
                                            AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
                                            
                                            [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
                                            
                                            AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
                                            
                                            if([paginatedOutput.items count]==0){
                                                                        
                                              AWSDynamoDBObjectMapper *dynamoDBObjectMapper1 = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
                                              AWSDynamoDBQueryExpression *queryExpression1 = [AWSDynamoDBQueryExpression new];
                                                
                                            
                                                
                                            queryExpression1.hashKeyAttribute = @"ID";
                                            queryExpression1.hashKeyValues = credentialsProvider.identityId;
                                                                        
                                            [[dynamoDBObjectMapper1 query:[User class]
                                                                    expression:queryExpression1]
                                                                    continueWithBlock:^id(AWSTask *task) {
                                                                             
                                                if (task.error) {
                                                    NSLog(@"The request failed. Error: [%@]", task.error);
                                                }
                                                if (task.exception) {
                                                NSLog(@"The request failed. Exception: [%@]", task.exception);
                                                }
                                                if (task.result) {
                                                                                 
                                                AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
                                                                                 
                                                                                 
                                                for(User *user in paginatedOutput.items){
                                                                                     
                                                                                     
                                                //indication that last name is still empty too
                                                        if([user.FirstName isEqualToString:@"NULL"]){
                                                           AWSIdentityManager *identityManager = [AWSIdentityManager sharedInstance];
                                                        NSArray *name = [identityManager.userName componentsSeparatedByString:@" "];
                                                          if([name count]==1){
                                                            user.FirstName = [name objectAtIndex:0];
                                                          }
                                                          else if([name count]>1){
                                                            user.FirstName = [name objectAtIndex:0];
                                                    user.LastName = [name objectAtIndex:([name count]-1)];
                                                          }
                                                        }
                                                        user.FBID = [FBSDKAccessToken currentAccessToken].userID;
                                                        user.Login = true;
                                                        [[dynamoDBObjectMapper1 save:user]
                                                                                continueWithBlock:^id(AWSTask *task) {
                                                            if (task.error) {
                                                                NSLog(@"The request failed. Error: [%@]", task.error);
                                                            }
                                                            if (task.exception) {
                                                                NSLog(@"The request failed. Exception: [%@]", task.exception);
                                                                                          }
                                                            if (task.result) {
                                                                //Do something with the result.
                                                            }
                                                            return nil;
                                                        }];
                                                                                     
                                                    }
                                                }
                                                return nil;
                                             }];
                                           }
                                            else{
                                                
                                            
                                                for(User *user in paginatedOutput.items){
                                                        user.Login = true;
                                                       [[dynamoDBObjectMapper save:user]
                                                     continueWithBlock:^id(AWSTask *task) {
                                                         if (task.error) {
                                                             NSLog(@"The request failed. Error: [%@]", task.error);
                                                         }
                                                         if (task.exception) {
                                                             NSLog(@"The request failed. Exception: [%@]", task.exception);
                                                         }
                                                         if (task.result) {
                                                             //Do something with the result.
                                                         }
                                                         return nil;
                                                     }];
                                                }
                                            }
                                         }
                                 return nil;
                              }];
                     });
                 }
             }];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ControlPanelNavigation *controlVC = [storyboard instantiateViewControllerWithIdentifier:@"controlPanelNavigator"];
    controlVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    ControlPanelViewController *controlPanelVC = (ControlPanelViewController *)controlVC.topViewController;
    if(progressIndicator!=nil){
        
        controlPanelVC.progressIndicator = progressIndicator;
        
    }
  
    controlPanelVC.login.hidden = YES;
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [self presentViewController:controlVC animated:YES completion:nil];
    });
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showRegistration"]) {
        
        // Get reference to the destination view controller
        RegistrationViewController *vc = [segue destinationViewController];
        if(progressIndicator!=nil){
            
            vc.progressIndicator = progressIndicator;
            
        }
        
    }}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
@end

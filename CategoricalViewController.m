//
//  CategoricalViewController.m
//  App
//
//  Created by Internet Wu on 07/09/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

//TODO: Load Buttons at the same time as images, and only display them after image has been displayed
//Currently download starts after image is displayed.


//TODO: File System seems to only store the Homepage. This is due to the fact that the rest has a name like "Brands/Brands.png". The "/" creates problem, which should be solved when we store images using their ids.
//TODO: Check disk space (there is a funtion for that on Stack Overflow) before adding images to disk


#import "CategoricalViewController.h"

@interface CategoricalViewController ()


@end

@implementation CategoricalViewController
@synthesize passedButton;
@synthesize progressIndicator;
@synthesize loadingCircle;
@synthesize startTime;
static NSString * const cloudfrontDomain = @"http://dxogq6khllfaf.cloudfront.net/";

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",[[HistoryCache sharedCache] objectForKey:@"Brands/Brands.png"]);
    [self createSwipes];
    //show a fleeting waiting screen
    [self.view setBackgroundColor: [UIColor whiteColor]];
    loadingCircle = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [loadingCircle setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width/2)-50, ([UIScreen mainScreen].bounds.size.height/2)-50, 100, 100)];
    [self.view addSubview:loadingCircle];
    [loadingCircle startAnimating];
    [loadingCircle setHidesWhenStopped:YES];
    
    // Do any additional setup after loading the view.
    if(passedButton != nil){
        
        [self makeImageWithPath: [progressIndicator stringByAppendingString:[passedButton.name stringByAppendingString:@".png"]]];

    }
    else{
        if([HistoryMemory isEmpty]||[[[HistoryMemory sharedHistory] objectAtIndex:[HistoryMemory getSharedIndex]] isEqualToString:@""]){
        
            [self makeImageWithPath:@"Homepage.png"];
        }
        else{
           NSArray *splitHistory = [[[HistoryMemory sharedHistory] objectAtIndex:[HistoryMemory getSharedIndex]] componentsSeparatedByString:@"/"];

            //-2 due to the fact that the last element is always an empty string
            [self makeImageWithPath:[[[[HistoryMemory sharedHistory] objectAtIndex:[HistoryMemory getSharedIndex]] stringByAppendingString:[splitHistory objectAtIndex:[splitHistory count]-2]] stringByAppendingString:@".png"]];
        }
        
    }
   

 
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) btnTouched:(SpecialButton* )sender {
   
    if([sender.type isEqualToString:@"CP"]){
        
        CategoricalViewController *categoricalVC = [[CategoricalViewController alloc] init];
        categoricalVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        NSString* empty = @"";
        if(progressIndicator!=nil){
        categoricalVC.progressIndicator = [empty stringByAppendingString: progressIndicator];
        categoricalVC.progressIndicator = [categoricalVC.progressIndicator stringByAppendingString: sender.name];
        categoricalVC.progressIndicator = [categoricalVC.progressIndicator stringByAppendingString: @"/"];
        }
        else{
        categoricalVC.progressIndicator = [empty stringByAppendingString: sender.name];
        categoricalVC.progressIndicator = [categoricalVC.progressIndicator stringByAppendingString: @"/"];
        }
        if(![HistoryMemory isEmpty]){
            if([HistoryMemory getSharedIndex]!= ((int)[[HistoryMemory sharedHistory] count]-1)){
                for(int i = ((int)[[HistoryMemory sharedHistory] count]-1);i>[HistoryMemory getSharedIndex];i--){
                  dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), ^{
                    //Clear Cache
                    NSArray *splitHistory = [[[HistoryMemory sharedHistory] objectAtIndex:i] componentsSeparatedByString:@"/"];
                    NSString *path = [[[[HistoryMemory sharedHistory] objectAtIndex:i] stringByAppendingString:[splitHistory objectAtIndex:[splitHistory count]-2]] stringByAppendingString: @".png"];
                    [[HistoryCache sharedCache] removeObjectForKey:path];
                    
                    //Delete images in Library/Caches
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                    NSString *cachesDirectory = [paths objectAtIndex:0];
                    
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    if([fileManager fileExistsAtPath:[cachesDirectory stringByAppendingString:path]]){
                        
                        [fileManager removeItemAtPath:[cachesDirectory stringByAppendingString:path] error:nil];
                    }
                    [[HistoryMemory sharedHistory] removeObjectAtIndex:i];
                    [[HistoryMemory sharedCategories] removeObjectAtIndex:i];
                  });
                }
            }
            [[HistoryMemory sharedHistory] addObject:categoricalVC.progressIndicator];
            [[HistoryMemory sharedCategories] addObject:@"CP"];
            [HistoryMemory setSharedIndex:[HistoryMemory getSharedIndex]+1];
        }
        else{
      
            //initialized the array
            [HistoryMemory sharedHistory];
            [[HistoryMemory sharedHistory] addObject:@""];
            [[HistoryMemory sharedCategories] addObject:@"CP"];
            [[HistoryMemory sharedHistory] addObject:categoricalVC.progressIndicator];
            [[HistoryMemory sharedCategories] addObject:@"CP"];
            [HistoryMemory setSharedIndex:1];
            
        }
        categoricalVC.passedButton = [[Button alloc] init];
        categoricalVC.passedButton.x = sender.x;
        categoricalVC.passedButton.y = sender.y;
        categoricalVC.passedButton.width = sender.width;
        categoricalVC.passedButton.height = sender.height;
        categoricalVC.passedButton.type = sender.type;
        categoricalVC.passedButton.name = sender.name;
        categoricalVC.passedButton.page = sender.page;
        categoricalVC.passedButton.brand = sender.brand;
        [self presentViewController:categoricalVC animated:YES completion:nil];
        
    }
    else if([sender.type isEqualToString:@"PP"]){
        
    }
    else if([sender.type isEqualToString:@"PL"]){
        
        
        ListCollectionViewController *listVC = [[ListCollectionViewController alloc] init];
        listVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        NSString* empty = @"";
        listVC.progressIndicator = [empty stringByAppendingString: progressIndicator];
        listVC.progressIndicator = [listVC.progressIndicator stringByAppendingString: sender.name];
        listVC.progressIndicator = [listVC.progressIndicator stringByAppendingString: @"/"];
        if([HistoryMemory getSharedIndex]!= ((int)[[HistoryMemory sharedHistory] count]-1)){
            for(int i = ((int)[[HistoryMemory sharedHistory] count]-1);i>[HistoryMemory getSharedIndex];i--){
                dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), ^{
                    //Clear Cache
                    NSArray *splitHistory = [[[HistoryMemory sharedHistory] objectAtIndex:i] componentsSeparatedByString:@"/"];
                    NSString *path = [[[[HistoryMemory sharedHistory] objectAtIndex:i] stringByAppendingString:[splitHistory objectAtIndex:[splitHistory count]-2]] stringByAppendingString: @".png"];
                    [[HistoryCache sharedCache] removeObjectForKey:path];
                    
                    //Delete images in Library/Caches
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                    NSString *cachesDirectory = [paths objectAtIndex:0];
                    
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    if([fileManager fileExistsAtPath:[cachesDirectory stringByAppendingString:path]]){
                        
                        [fileManager removeItemAtPath:[cachesDirectory stringByAppendingString:path] error:nil];
                    }
                    [[HistoryMemory sharedHistory] removeObjectAtIndex:i];
                    [[HistoryMemory sharedCategories] removeObjectAtIndex:i];
                });
                
            }
        }
        [[HistoryMemory sharedHistory] addObject:listVC.progressIndicator];
        [[HistoryMemory sharedCategories] addObject:@"PL"];
        [HistoryMemory setSharedIndex:[HistoryMemory getSharedIndex]+1];
       
        listVC.passedButton = [[Button alloc] init];
        listVC.passedButton.x = sender.x;
        listVC.passedButton.y = sender.y;
        listVC.passedButton.width = sender.width;
        listVC.passedButton.height = sender.height;
        listVC.passedButton.type = sender.type;
        listVC.passedButton.name = sender.name;
        listVC.passedButton.page = sender.page;
        listVC.passedButton.brand = sender.brand;
     
        [self presentViewController:listVC animated:YES completion:nil];
    }
    else if([sender.type isEqualToString:@"IP"]){
        
    }

}

-(void)handleSwipeGesture:(UISwipeGestureRecognizer *) sender
{
    
    if(sender.direction==UISwipeGestureRecognizerDirectionDown){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ControlPanelNavigation *controlVC = [storyboard instantiateViewControllerWithIdentifier:@"controlPanelNavigator"];
            //controlVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        controlVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
        
        ControlPanelViewController *controlPanelVC = (ControlPanelViewController *)controlVC.topViewController;

            if(progressIndicator!=nil){
            
                controlPanelVC.progressIndicator = progressIndicator;

            }
        if ([FBSDKAccessToken currentAccessToken]) {
          
            controlPanelVC.login.hidden = YES;
            
            
        }
        else{
         

            controlPanelVC.FBlogin.hidden = YES;
    
            AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                                                                  initWithRegionType:AWSRegionUSEast1
                                                                  identityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
            
            AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
            
            [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
            
            AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
            AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
            
            queryExpression.hashKeyAttribute = @"ID";
            queryExpression.hashKeyValues = credentialsProvider.identityId;
      
            [[dynamoDBObjectMapper query:[User class]
                              expression:queryExpression]
             continueWithBlock:^id(AWSTask *task) {
                 
                 if (task.error) {
                     NSLog(@"The request failed. Error: [%@]", task.error);
                 }
                 if (task.exception) {
                     NSLog(@"The request failed. Exception: [%@]", task.exception);
                 }
                 if (task.result) {
                     
                     AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
                     for(User *user in paginatedOutput.items){
                      if(user.Login==true||user.Login==1){
                      
                       controlPanelVC.loginText = @"Logged In";
                         
                      }
                     }
                 }
                 return nil;
             }];
            
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:controlVC animated:YES completion:nil];
        });

    }
    else if(sender.direction==UISwipeGestureRecognizerDirectionRight){
        if(![HistoryMemory isEmpty]){
            if([HistoryMemory getSharedIndex]!=0){

                [HistoryMemory setSharedIndex: [HistoryMemory getSharedIndex]-1];
                if([[[HistoryMemory sharedCategories] objectAtIndex:[HistoryMemory getSharedIndex]] isEqualToString:@"CP"]){
                    CategoricalViewController *vc = [[CategoricalViewController alloc] init];
                    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    NSString *empty = @"";
                    if([[HistoryMemory sharedHistory] objectAtIndex:[HistoryMemory getSharedIndex]]!=nil){
                        vc.progressIndicator = [empty stringByAppendingString: [[HistoryMemory sharedHistory]objectAtIndex:[HistoryMemory getSharedIndex]]];
                    }
                    [self presentViewController:vc animated:YES completion:nil];
                }
                else if([[[HistoryMemory sharedCategories] objectAtIndex:[HistoryMemory getSharedIndex]] isEqualToString:@"PL"]){
                    ListCollectionViewController *vc = [[ListCollectionViewController alloc] init];
                    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    NSString *empty = @"";
                    if([[HistoryMemory sharedHistory] objectAtIndex:[HistoryMemory getSharedIndex]]!=nil){
                        vc.progressIndicator = [empty stringByAppendingString: [[HistoryMemory sharedHistory]objectAtIndex:[HistoryMemory getSharedIndex]]];
                    }
                    [self presentViewController:vc animated:YES completion:nil];
                }
                
                
               
            }
        }
    }
    else if(sender.direction==UISwipeGestureRecognizerDirectionLeft){
        if(![HistoryMemory isEmpty]){
            if([HistoryMemory getSharedIndex] !=(int)[[HistoryMemory sharedHistory] count]-1){
                [HistoryMemory setSharedIndex: [HistoryMemory getSharedIndex]+1];
                if([[[HistoryMemory sharedCategories] objectAtIndex:[HistoryMemory getSharedIndex]] isEqualToString:@"CP"]){
                    CategoricalViewController *vc = [[CategoricalViewController alloc] init];
                    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    NSString *empty = @"";
                    if([[HistoryMemory sharedHistory] objectAtIndex:[HistoryMemory getSharedIndex]]!=nil){
                        vc.progressIndicator = [empty stringByAppendingString: [[HistoryMemory sharedHistory]objectAtIndex:[HistoryMemory getSharedIndex]]];
                    }
                    [self presentViewController:vc animated:YES completion:nil];
                }
                else if([[[HistoryMemory sharedCategories] objectAtIndex:[HistoryMemory getSharedIndex]] isEqualToString:@"PL"]){
                    ListCollectionViewController *vc = [[ListCollectionViewController alloc] init];
                    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    NSString *empty = @"";
                    if([[HistoryMemory sharedHistory] objectAtIndex:[HistoryMemory getSharedIndex]]!=nil){
                        vc.progressIndicator = [empty stringByAppendingString: [[HistoryMemory sharedHistory]objectAtIndex:[HistoryMemory getSharedIndex]]];
                    }
                    [self presentViewController:vc animated:YES completion:nil];
                }
            }
        }
    }
}

- (void)createButtons{
    
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
    
    queryExpression.indexName = @"page-index";
    queryExpression.hashKeyAttribute = @"page";
    //this corresponds to the page we are currently on
    if(passedButton!=nil){
        queryExpression.hashKeyValues = passedButton.name;
    }
    else{
        if([HistoryMemory isEmpty]||[[[HistoryMemory sharedHistory] objectAtIndex:[HistoryMemory getSharedIndex]] isEqualToString:@""]){
            queryExpression.hashKeyValues = @"Homepage";
        }
        else{
           NSArray *splitHistory = [[[HistoryMemory sharedHistory] objectAtIndex:[HistoryMemory getSharedIndex]] componentsSeparatedByString:@"/"];
            queryExpression.hashKeyValues = [splitHistory objectAtIndex:[splitHistory count]-2];
        }
    }
    
    [[dynamoDBObjectMapper query:[Button class]
                      expression:queryExpression]
     continueWithBlock:^id(AWSTask *task) {
         
         if (task.error) {
             NSLog(@"The request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             
             for (Button *button in paginatedOutput.items) {
                 
                 if(passedButton !=nil){
                     if([passedButton.brand isEqualToString:@"NULL"]){
                         if([button.type isEqualToString:@"CP"]){
                             [self createButtonWithX: [button.x floatValue] withY: [button.y floatValue] withWidth:[button.width floatValue]  withHeight: [button.height floatValue] withType:button.type withBrand:button.brand withName:button.name withPage:button.page];
                         }
                     }
                     else{
                         if(([button.type isEqualToString:@"CP"]&&[button.brand isEqualToString:passedButton.brand])||[button.type isEqualToString:@"PL"]){
                             
                             [self createButtonWithX: [button.x floatValue] withY: [button.y floatValue] withWidth:[button.width floatValue]  withHeight: [button.height floatValue] withType:button.type withBrand:button.brand withName:button.name withPage:button.page];
                         }
                     }
                 }
                 else{
                     
                     if([button.type isEqualToString:@"CP"]||[button.type isEqualToString:@"PL"]){
                         
                         [self createButtonWithX: [button.x floatValue] withY: [button.y floatValue] withWidth:[button.width floatValue]  withHeight: [button.height floatValue] withType:button.type withBrand:button.brand withName:button.name withPage:button.page];
                     }
                 }
             }
             
         }
         
         return nil;
     }];
}

- (void)createSwipes{
    UISwipeGestureRecognizer *swipeDownGesture=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    [self.view addGestureRecognizer:swipeDownGesture];
    swipeDownGesture.direction=UISwipeGestureRecognizerDirectionDown;
    
    UISwipeGestureRecognizer *swipeUpGesture=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    [self.view addGestureRecognizer:swipeUpGesture];
    swipeUpGesture.direction=UISwipeGestureRecognizerDirectionUp;
    
    UISwipeGestureRecognizer *swipeLeftGesture=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    [self.view addGestureRecognizer:swipeLeftGesture];
    swipeLeftGesture.direction=UISwipeGestureRecognizerDirectionLeft;
    
    UISwipeGestureRecognizer *swipeRightGesture=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    [self.view addGestureRecognizer:swipeRightGesture];
    swipeRightGesture.direction=UISwipeGestureRecognizerDirectionRight;
}

- (void)makeImageWithPath:(NSString *)path{
   
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 375, 667)];
    
    //Check if image exists in Cache
    if([[HistoryCache sharedCache] objectForKey:path]!=nil){
        NSLog(@"cache");
        [imageView setImage: [[HistoryCache sharedCache] objectForKey:path]];
        [self createScrollViewWith:imageView];
        [loadingCircle stopAnimating];
        
        
        [self createButtons];
        return;
    }
    else{
            //Check if image exists in /Library/Caches Directory
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *cachesDirectory = [paths objectAtIndex:0];
     
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if([fileManager fileExistsAtPath:[[cachesDirectory stringByAppendingString:@"/"] stringByAppendingString:path]]){
            NSLog(@"File");
            [imageView setImage:[UIImage imageWithData:
                                 [fileManager contentsAtPath:[[cachesDirectory stringByAppendingString:@"/"] stringByAppendingString:path] ]]];
            [self createScrollViewWith:imageView];
            [loadingCircle stopAnimating];
            
            
            [self createButtons];
            return;
        }
    }
    
    //It is important to replace spaces with %20
    NSString *correctedPath = [path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSURL *url = [NSURL URLWithString:
                  [cloudfrontDomain stringByAppendingString:correctedPath]];
    
    
    
    //asynchronous download with update to the main thread after download
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), ^{
  
        startTime = CFAbsoluteTimeGetCurrent();
        NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                       downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                        
                                                           NSLog(@"Network");
                                                           UIImage *downloadedImage = [UIImage imageWithData:
                                                                                       [NSData dataWithContentsOfURL:location]];
                                                           
                                                           NSLog(@"%f",(CFAbsoluteTimeGetCurrent()-startTime)*1000);
                                                           
                                                           if (!imageView) return;
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               
                                                               [imageView setImage:downloadedImage];
                                                               [self createScrollViewWith:imageView];
                                                               [loadingCircle stopAnimating];
                                                              
                                                               
                                                               [self createButtons];
                                                           });
                                                           
                                                           //Put the image in the Memory Cache
                                                           if([[HistoryCache sharedCache] objectForKey:path]==nil){
                                                               
                                                                [[HistoryCache sharedCache] setObject:downloadedImage forKey:path];
                                                           }
                                                           //Put it also in the /Library/Caches folder on disk
                                                           NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                                                           NSString *cachesDirectory = [paths objectAtIndex:0];
                                                           
                                                           NSFileManager *fileManager = [NSFileManager defaultManager];
                                                           
                                                           
                                                           [fileManager createFileAtPath:[[cachesDirectory stringByAppendingString:@"/"] stringByAppendingString:path] contents:[NSData dataWithContentsOfURL:location] attributes:nil];
                                                           
                                                    
                                                           
                                                           
                                                       }];
        [downloadPhotoTask resume];
    });

}

- (void)createScrollViewWith:(UIImageView *)imageView{
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame: self.view.frame];
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    scrollView.contentSize = CGSizeMake(375,imageView.frame.size.height);
    [scrollView addSubview:imageView];
    
    [self.view addSubview:scrollView];
}

- (void)createButtonWithX:(CGFloat)x withY:(CGFloat)y withWidth:(CGFloat)width withHeight:(CGFloat)height withType:(NSString *)type withBrand:(NSString *)brand withName:(NSString *)name withPage:(NSString *)page{
    
   SpecialButton *button = [SpecialButton buttonWithType:UIButtonTypeRoundedRect];
  
    button.x = @(x);
    button.y = @(y);
    button.width = @(width);
    button.height = @(height);
    button.type = type;
    button.brand = brand;
    button.name = name;
    button.page = page;
    [button addTarget:self
               action:@selector(btnTouched:)
    forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(x, y, width, height);
    dispatch_async(dispatch_get_main_queue(), ^{
    [self.view addSubview:button];
    });
}

@end

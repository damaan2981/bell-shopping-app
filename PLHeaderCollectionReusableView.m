//
//  PLHeaderCollectionReusableView.m
//  App
//
//  Created by Internet Wu on 25/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import "PLHeaderCollectionReusableView.h"

@implementation PLHeaderCollectionReusableView
@synthesize headerImageView;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //Initialize views
        headerImageView = [UIImageView new];
        
        //add the subviews to the cell
        [self addSubview:headerImageView];
        
        
    }
    return self;
}
@end

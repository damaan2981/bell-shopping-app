//
//  HistoryCache.m
//  App
//
//  Created by Internet Wu on 18/11/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

//Stores images in the History Memory

#import "HistoryCache.h"

static HistoryCache *sharedMyAppCache = nil;

@implementation HistoryCache

+ (HistoryCache *) sharedCache {
    if (sharedMyAppCache == nil) {
        NSLog(@"A new Cache is created in Memory");
        sharedMyAppCache = [[self alloc] init];
    }
    return sharedMyAppCache;
}



@end

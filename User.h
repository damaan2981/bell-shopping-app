//
//  User.h
//  App
//
//  Created by Internet Wu on 03/09/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

#import "AWSDynamoDBObjectMapper.h"
#import <UIKit/UIKit.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import <Foundation/Foundation.h>
#import "FBSDKLoginKit/FBSDKLoginKit.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
@interface User : AWSDynamoDBObjectModel

@property (nonatomic, strong) NSString *Email;
@property (nonatomic, strong) NSString *Password;
@property (nonatomic, strong) NSString *FirstName;
@property (nonatomic, strong) NSString *LastName;
@property (nonatomic, strong) NSString *ID;
@property (nonatomic) Boolean Login;
//Facebook ID
@property (nonatomic, strong) NSString *FBID;
@end

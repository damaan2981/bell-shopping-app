//
//  ProductList.h
//  App
//
//  Created by Internet Wu on 24/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductItem.h"
@interface ProductList : NSObject
@property (nonatomic, strong) NSMutableArray *productItems;
- (void)addToItems:(ProductItem *)item;
- (NSMutableArray *)getItems;
@end

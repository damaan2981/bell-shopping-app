//
//  ListCollectionViewController.h
//  App
//
//  Created by Internet Wu on 26/09/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AWSS3/AWSS3.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import "Button.h"
#import "SpecialButton.h"
#import "CategoricalViewController.h"
#import "ProductList.h"
#import "ProductItem.h"
#import "PLHeaderCollectionReusableView.h"
#import "CustomCollectionViewCell.h"
#import "ProductViewController.h"
#import "PendingOperations.h"
#import "ImageDownloader.h"
#import "HistoryMemory.h"
@interface ListCollectionViewController : UICollectionViewController <ImageDownloaderDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) Button *passedButton;
@property (nonatomic, strong) NSMutableArray *buttons;
@property (nonatomic, strong) NSString *progressIndicator;
@property (nonatomic, strong) NSMutableArray *photos; // main data source of controller
@property (nonatomic, strong) ProductList *productList;
@property (nonatomic) CGFloat screenWidth;
@property (nonatomic, strong) UIActivityIndicatorView *loadingCircle;
//Used to track Pending Operations
@property (nonatomic, strong) PendingOperations *pendingOperations;
@property (nonatomic, strong) NSMutableDictionary *indicatorDictionary;
//Used to track previous indexPath Position
@property (nonatomic, strong) NSIndexPath *historyIndexPathMin;
@property (nonatomic, strong) NSIndexPath *historyIndexPathMax;
@property (nonatomic, strong) UIImage *headerPicture;
@property (nonatomic) double startTime;
@end

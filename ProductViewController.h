//
//  ProductViewController.h
//  App
//
//  Created by Internet Wu on 26/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Stripe.h"
#import "ProductItem.h"
#import <PassKit/PassKit.h>
@interface ProductViewController : UIViewController
<PKPaymentAuthorizationViewControllerDelegate>
@property (nonatomic, strong) ProductItem *item;
@end

//
//  HistoryMemory.m
//  App
//
//  Created by Internet Wu on 18/11/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import "HistoryMemory.h"

static NSMutableArray *sharedMyAppHistory = nil;
//to keep track of where we are in the HistoryMemory
static int historyIndex = 0;
static NSMutableArray *sharedHistoryCategories = nil;

@implementation HistoryMemory

+ (NSMutableArray *) sharedHistory {
    if (sharedMyAppHistory == nil) {
        sharedMyAppHistory = [[NSMutableArray alloc] init];
        sharedHistoryCategories = [[NSMutableArray alloc] init];
    }
    return sharedMyAppHistory;
}

+ (int) getSharedIndex {
    return historyIndex;
}

+ (NSMutableArray *)sharedCategories{
    return sharedHistoryCategories;
}

+ (void) setSharedIndex:(int) index{
    historyIndex = index;
}

+ (BOOL)isEmpty {
    if([sharedMyAppHistory count]==0){
        return true;
    }
    return false;
}

@end

//
//  HistoryMemory.h
//  App
//
//  Created by Internet Wu on 18/11/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HistoryMemory : NSObject


//This class method is used to access the Singleton Object
+ (NSMutableArray *) sharedHistory;
+ (int) getSharedIndex;
+ (void) setSharedIndex:(int) index;
+ (NSMutableArray *) sharedCategories;
+ (BOOL)isEmpty;
@end

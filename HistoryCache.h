//
//  HistoryCache.h
//  App
//
//  Created by Internet Wu on 18/11/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

//This class is implemented using the Singleton Design Pattern
//HistoryCache is evicted when user enters background. This is good, as the background apps that consume the most memory, are terminated first
//Loading images from HistoryCache takes approximately 0.5 milliseconds, while loading from disk takes approximately 3 milliseconds

#import <Foundation/Foundation.h>

@interface HistoryCache : NSCache

//This class method is used to access the Singleton Object
+ (HistoryCache *) sharedCache;
@end

//
//  ProductList.m
//  App
//
//  Created by Internet Wu on 24/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import "ProductList.h"

@implementation ProductList
@synthesize productItems;

- (id) init
{
    self = [super init];
    productItems = [[NSMutableArray alloc] init];
    return self;
}

- (void)addToItems:(ProductItem *)item{
    [productItems addObject:item];
}

- (NSMutableArray *)getItems{
    return productItems;
}

@end

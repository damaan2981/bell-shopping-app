//
//  PaymentViewController.m
//  App
//
//  Created by Internet Wu on 27/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//
//  Implements Stripe Functionality

//!!For testing purposes, enter 4242 4242 4242 4242 into the credit card field.
//Else the other fields (expiration date and cvc) will not appear

#import "PaymentViewController.h"

@interface PaymentViewController ()<STPPaymentCardTextFieldDelegate>

//The field for credit card number, expiration date and cvc
@property(nonatomic) STPPaymentCardTextField *paymentTextField;
@end

@implementation PaymentViewController
@synthesize paymentTextField;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.paymentTextField = [[STPPaymentCardTextField alloc] initWithFrame:CGRectMake(15, 15, CGRectGetWidth(self.view.frame) - 30, 44)];
    
    
    self.paymentTextField.delegate = self;
   
    [self.view addSubview:self.paymentTextField];

   
}

- (void)paymentCardTextFieldDidChange:(STPPaymentCardTextField *)textField {
    
    if (textField.isValid) {
        
        //assigning relevant parameters to a STPCardParams object
        STPCardParams *card = [[STPCardParams alloc] init];
        card.number = textField.cardNumber;
        card.expMonth = textField.expirationMonth;
        card.expYear = textField.expirationYear;
        card.cvc = textField.cvc;
       // [self doSomethingWithCard:card];
    }
       // self.saveButton.enabled = textField.isValid;
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

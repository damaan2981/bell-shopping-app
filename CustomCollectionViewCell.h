//
//  CustomCollectionViewCell.h
//  App
//
//  Created by Internet Wu on 25/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewCell : UICollectionViewCell {
    UIImageView *imageView;
    UILabel *nameLabel;
    UILabel *priceLabel;
}

//Two Methods returning a UIColor from a hex format integer (0x...) the hex from the database should have this format
//RGB color macro
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]



@property (nonatomic,retain) UIImageView *imageView;
@property (nonatomic, retain)  UILabel *nameLabel;
@property (nonatomic, retain)  UILabel *priceLabel;


@end

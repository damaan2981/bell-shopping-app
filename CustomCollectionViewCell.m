//
//  CustomCollectionViewCell.m
//  App
//
//  Created by Internet Wu on 25/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import "CustomCollectionViewCell.h"

@implementation CustomCollectionViewCell
@synthesize imageView;
@synthesize nameLabel;
@synthesize priceLabel;
-(void)awakeFromNib{
    
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //Initialize views
        imageView = [UIImageView new]; //new is a short hand that performs alloc and init at the same time,
        nameLabel = [UILabel new];
        priceLabel = [UILabel new];
        
        //add the subviews to the cell
        [self.contentView addSubview:imageView];
        [self.contentView addSubview:nameLabel];
        [self.contentView addSubview:priceLabel];
        
        
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
}

@end

//
//  PLHeaderCollectionReusableView.h
//  App
//
//  Created by Internet Wu on 25/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PLHeaderCollectionReusableView : UICollectionReusableView{
    UIImageView *headerImageView;
}
@property(retain, nonatomic) UIImageView *headerImageView;
@end

//
//  ImageDownloader.m
//  App
//
//  Created by Internet Wu on 13/11/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import "ImageDownloader.h"

//Declares a private interface, so that we can change the properties to read-write
@interface ImageDownloader ()
@property (nonatomic, readwrite, strong) NSIndexPath *indexPathInCollectionView;
@property (nonatomic, readwrite, strong) ProductItem *productItem;
@end

@implementation ImageDownloader
@synthesize delegate = _delegate;
@synthesize indexPathInCollectionView = _indexPathInCollectionView;
@synthesize productItem = _productItem;


- (id)initWithProductItem:(ProductItem *)item atIndexPath:(NSIndexPath *)indexPath delegate:(id<ImageDownloaderDelegate>)theDelegate {
    
    if (self = [super init]) {
        
        // Set the properties
        self.delegate = theDelegate;
        self.indexPathInCollectionView = indexPath;
        self.productItem = item;
    }
    return self;
}

#pragma mark -
#pragma mark - Downloading image


- (void)main {
    
    //NSAutoreleasepool is used in Cocoa for memory management
    @autoreleasepool {
        
        if (self.isCancelled)
            return;
        
        NSData *imageData = [[NSData alloc] initWithContentsOfURL:self.productItem.URL];
        
        if (self.isCancelled) {
            imageData = nil;
            return;
        }
        
        if (imageData) {
            UIImage *downloadedImage = [UIImage imageWithData:imageData];
            self.productItem.picture = downloadedImage;
        }
        else {
            self.productItem.failed = YES;
        }
        
        imageData = nil;
        
        if (self.isCancelled)
            return;
        
        // Notify the caller on the main thread
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(imageDownloaderDidFinish:) withObject:self waitUntilDone:NO];
        
    }
}


@end

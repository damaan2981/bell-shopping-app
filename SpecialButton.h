//
//  SpecialButton.h
//  App
//
//  Created by Internet Wu on 16/09/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialButton : UIButton
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *page;
@property (nonatomic, strong) NSString *brand;
@property (nonatomic, strong) NSNumber *x;
@property (nonatomic, strong) NSNumber *y;
@property (nonatomic, strong) NSNumber *height;
@property (nonatomic, strong) NSNumber *width;
@property (nonatomic, strong) NSString *type;
@end

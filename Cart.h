//
//  Cart.h
//  App
//
//  Created by Internet Wu on 29/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

//Stores the ProductItems in the Cart

#import <Foundation/Foundation.h>
#import "ProductItem.h"

@interface Cart : NSObject
@property (nonatomic, strong) NSMutableArray *cartItems;
- (void)addItemToCart:(ProductItem *)item;
- (NSMutableArray *)getItems;
@end

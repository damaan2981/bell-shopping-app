//
//  CategoricalViewController.h
//  App
//
//  Created by Internet Wu on 07/09/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AWSS3/AWSS3.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import "Button.h"
#import "SpecialButton.h"
#import "ListCollectionViewController.h"
#import "ControlPanelNavigation.h"
#import "ControlPanelViewController.h"
#import <Foundation/Foundation.h>
#import "HistoryCache.h"
#import "HistoryMemory.h"

@interface CategoricalViewController : UIViewController
@property (nonatomic, strong) Button *passedButton;
//might later be replaced entirely by the HistoryMemory
@property (nonatomic, strong) NSString *progressIndicator;
@property (nonatomic, strong) UIActivityIndicatorView *loadingCircle;
@property (nonatomic) double startTime;
- (void)createButtons;
@end

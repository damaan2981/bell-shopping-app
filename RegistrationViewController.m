//
//  RegistrationViewController.m
//  App
//
//  Created by Internet Wu on 03/09/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

//Make stuff for Amazon Cognito!!!


#import "RegistrationViewController.h"
#import <AWSCognito/AWSCognito.h>
#import <AWSCore/AWSCore.h>

@interface RegistrationViewController ()
@property (weak, nonatomic) IBOutlet UITextField *fNText;

@property (weak, nonatomic) IBOutlet UITextField *lNText;
@property (weak, nonatomic) IBOutlet UITextField *emText;
@property (weak, nonatomic) IBOutlet UITextField *paText;
@property (weak, nonatomic) IBOutlet UIButton *regButton;

@end

@implementation RegistrationViewController

@synthesize fNText;
@synthesize lNText;
@synthesize emText;
@synthesize paText;
@synthesize regButton;
@synthesize progressIndicator;
- (void)viewDidLoad {
 
   
    [super viewDidLoad];
    self.fNText.delegate = self;
    self.lNText.delegate = self;
    self.emText.delegate = self;
    self.paText.delegate = self;
    [regButton addTarget:self
                  action:@selector(clicked:)
        forControlEvents:UIControlEventTouchUpInside];
    
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)clicked:(UIButton*)sender{
    
    if ([emText.text length] == 0 || [paText.text length] == 0 || [fNText.text length] == 0 || [lNText.text length] == 0) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Oops!"
                                      message:@"Make sure to fill in all boxes!"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
        AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
        queryExpression.indexName = @"Email-index";
        queryExpression.hashKeyAttribute = @"Email";
        queryExpression.hashKeyValues = emText.text;
        
        [[dynamoDBObjectMapper query:[User class]
                          expression:queryExpression]
         continueWithBlock:^id(AWSTask *task) {
             
             if (task.error) {
                 NSLog(@"The request failed. Error: [%@]", task.error);
             }
             if (task.exception) {
                 NSLog(@"The request failed. Exception: [%@]", task.exception);
             }
             if (task.result) {
                 
                 AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
                 
                 //user with the same email already exist
                 if([paginatedOutput.items count]>0){
                     NSLog(@"Already Registered");
                     
                 }
                 else{

                     AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                                                                           initWithRegionType:AWSRegionUSEast1
                                                                           identityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
                     
                     
                     AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
                     
                     [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
                     
                     AWSCognitoIdentity *identityClient = [AWSCognitoIdentity defaultCognitoIdentity];
                     AWSCognitoIdentityGetOpenIdTokenForDeveloperIdentityInput *idRequest = [[AWSCognitoIdentityGetOpenIdTokenForDeveloperIdentityInput alloc] init];
                     [idRequest setIdentityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
                     [idRequest setTokenDuration:@(60 * 5)];
                     [idRequest setIdentityId:credentialsProvider.identityId];
                     [idRequest setLogins:@{@"login.bellplatform.app": emText.text}];
                     
                    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
                     [[identityClient getOpenIdTokenForDeveloperIdentity:idRequest] continueWithBlock:^id(AWSTask *task) {
                         
                         if (task.error) {
                             NSLog(@"Error: %@", task.error);
                         }
                         else {
                             
                             //  AWSCognitoIdentityListIdentitiesResponse *listResp = task.result;
                             AWSCognitoIdentityGetOpenIdTokenForDeveloperIdentityResponse *responseId = task.result;
                             NSLog(@"%@",responseId);
                    [credentialsProvider setLogins:@{@"cognito-identity.amazonaws.com": responseId.token}];
                             AWSCognitoIdentityGetCredentialsForIdentityInput *credInput = [[AWSCognitoIdentityGetCredentialsForIdentityInput alloc]init];
                      
                             [credInput setIdentityId: responseId.identityId];
                             [credInput setLogins: credentialsProvider.logins];
                             [[identityClient getCredentialsForIdentity:credInput] continueWithBlock:^id(AWSTask *task) {
                                 if (task.error) {
                                     NSLog(@"Error: %@", task.error);
                                 }
                                 else if(task.result){
                                     AWSCognitoIdentityGetCredentialsForIdentityResponse *credResult = [[AWSCognitoIdentityGetCredentialsForIdentityResponse alloc]init];
                                    
                                 }
                                 dispatch_semaphore_signal(sema);
                                 return nil;
                             }];
                         }
                         return nil;
                     }];
                     while (dispatch_semaphore_wait(sema, DISPATCH_TIME_NOW)) { [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]]; }
                    
                  
                     //assigning the relevant values
                     User *user = [User new];
                     user.ID = credentialsProvider.identityId;
                     user.Email = emText.text;
                     user.Password = paText.text;
                     user.FirstName = fNText.text;
                     user.LastName = lNText.text;
                     user.Login = true;
                     user.FBID = @"NULL";
                     [[dynamoDBObjectMapper save:user]
                      continueWithBlock:^id(AWSTask *task) {
                          if (task.error) {
                              NSLog(@"The request failed. Error: [%@]", task.error);
                          }
                          if (task.exception) {
                              NSLog(@"The request failed. Exception: [%@]", task.exception);
                          }
                          if (task.result) {
                              //Do something with the result.
                              
                          }
                          return nil;
                      }];
                     
                   
                 }

                 
                 
                 
                //return to the control Panel upon registration
                if([paginatedOutput.items count]==0){
                 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                 ControlPanelNavigation *controlVC = [storyboard instantiateViewControllerWithIdentifier:@"controlPanelNavigator"];
                 controlVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                 ControlPanelViewController *controlPanelVC = (ControlPanelViewController *)controlVC.topViewController;
                    if(progressIndicator!=nil){
                     
                      controlPanelVC.progressIndicator = progressIndicator;
                        
                    }
                
                    controlPanelVC.FBlogin.hidden = YES;
                    controlPanelVC.loginText = @"Logged In";
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        [self presentViewController:controlVC animated:YES completion:nil];
                    });
                }
             }
             return nil;
         }];

        
    
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

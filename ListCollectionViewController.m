//
//  ListCollectionViewController.m
//  App
//
//  Created by Internet Wu on 26/09/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

//TODO:Sometimes, the progress Indicator keeps spinning, even though it is only created once!!!!!?????? and should be deleted whenever we set the image

#import <UIKit/UIKit.h>
#import "ListCollectionViewController.h"

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]
@interface ListCollectionViewController ()

@end

@implementation ListCollectionViewController
@synthesize productList;
@synthesize passedButton;
@synthesize progressIndicator;
@synthesize screenWidth;
@synthesize buttons;
@synthesize loadingCircle;
@synthesize photos = _photos;
@synthesize pendingOperations = _pendingOperations;
@synthesize indicatorDictionary;
@synthesize historyIndexPathMin;
@synthesize historyIndexPathMax;
@synthesize headerPicture;
@synthesize startTime;

static NSString * const reuseIdentifier = @"CustomCell";
static NSString * const cloudfrontDomain = @"http://dxogq6khllfaf.cloudfront.net/";

- (instancetype)init {
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
  
    
    return (self = [super initWithCollectionViewLayout:layout]);
}

- (PendingOperations *)pendingOperations {
    if (!_pendingOperations) {
        _pendingOperations = [[PendingOperations alloc] init];
    }
    return _pendingOperations;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    indicatorDictionary = [[NSMutableDictionary alloc] init];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    //Allow Swipe Gestures
    [self createSwipes];
    self.collectionView.backgroundColor = UIColorFromRGB(0xB58321); //Set the background for the hole collectionView
    loadingCircle = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [loadingCircle setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width/2)-50, ([UIScreen mainScreen].bounds.size.height/2)-50, 100, 100)];
    [self.view addSubview:loadingCircle];
    [loadingCircle startAnimating];
    [loadingCircle setHidesWhenStopped:YES];
    buttons = [[NSMutableArray alloc] init];
  
    productList = [[ProductList alloc] init];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    //1. get product names and URLs
    NSBlockOperation* buttonDownloadOp = [NSBlockOperation blockOperationWithBlock: ^{
        startTime = CFAbsoluteTimeGetCurrent();
        //We now need to determine the name of the relevant pictures that are going into the cells
        AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
        AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
        
        queryExpression.indexName = @"page-index";
        queryExpression.hashKeyAttribute = @"page";
        if(passedButton!=nil){
            queryExpression.hashKeyValues = passedButton.name;
        }
        else{
            
                NSArray *splitHistory = [[[HistoryMemory sharedHistory] objectAtIndex:[HistoryMemory getSharedIndex]] componentsSeparatedByString:@"/"];
            NSLog(@"%@",splitHistory);
                queryExpression.hashKeyValues = [splitHistory objectAtIndex:[splitHistory count]-2];
            
        }
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        [[dynamoDBObjectMapper query:[Button class]
                          expression:queryExpression]
         continueWithBlock:^id(AWSTask *task) {
             
             if (task.error) {
                 NSLog(@"The request failed. Error: [%@]", task.error);
             }
             if (task.exception) {
                 NSLog(@"The request failed. Exception: [%@]", task.exception);
             }
             if (task.result) {
                 
                 AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
                 for (int i = 0; i<[paginatedOutput.items count];i++) {
                     Button *button = [paginatedOutput.items objectAtIndex:i];
                     ProductItem *item = [[ProductItem alloc] init];
                     [item setProduct_name:button.name];
                     
                     NSString *temp = [progressIndicator stringByAppendingString:button.name];
                     temp = [temp stringByAppendingString:@".png"];
                     
                     //It is important to replace spaces with %20
                     NSString *correctedPath = [temp stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                     
                     NSURL *url = [NSURL URLWithString:
                                   [cloudfrontDomain stringByAppendingString:correctedPath]];
                     [item setURL:url];
                     [productList addToItems:item];
                     
                     if([button.type isEqualToString:@"PP"]){
                         [buttons addObject:button];
                         
                     }
                 }
                 
                 dispatch_semaphore_signal(semaphore);
                 
             }
             
             return nil;
         }];
        while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW)) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
        }
        
    }];
  
    //1. gets image names and calculate URLs
    [queue addOperation:buttonDownloadOp];
    
    //2 get header image
    NSBlockOperation* headerImageOp = [NSBlockOperation blockOperationWithBlock: ^{
        
        NSString *temp = [progressIndicator stringByAppendingString:@"headerImage.png"];
        
        //It is important to replace spaces with %20
        NSString *correctedPath = [temp stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        NSURL *url = [NSURL URLWithString:
                      [cloudfrontDomain stringByAppendingString:correctedPath]];
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                       downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                           
                                                           headerPicture = [UIImage imageWithData:
                                                                            [NSData dataWithContentsOfURL:location]];
                                                           
                                                           dispatch_semaphore_signal(semaphore);
                                                       }];
        [downloadPhotoTask resume];
        while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW)) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
        }
    }];
    
    //2. Get header Image
    [queue addOperation:headerImageOp];

   
    
    //3
    NSBlockOperation* firstImageOp = [NSBlockOperation blockOperationWithBlock: ^{
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                        downloadTaskWithURL:[[productList getItems][0] getProductURL] completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
         
                                                                  UIImage *downloadedImage = [UIImage imageWithData:
                                                                  [NSData dataWithContentsOfURL:location]];
         
                                                                 [[productList getItems][0] setPicture:downloadedImage];
         
                                                                  dispatch_semaphore_signal(semaphore);
                                                        }];
         [downloadPhotoTask resume];
         while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW)) {
         [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
         }
       
    }];
    
    [firstImageOp addDependency:buttonDownloadOp];
    //3. gets first image for flow layout item size configuration
    [queue addOperation:firstImageOp];
    
    
    //4
    NSBlockOperation* createLayoutOp = [NSBlockOperation blockOperationWithBlock: ^{
        NSLog(@"%f",(CFAbsoluteTimeGetCurrent()-startTime)*1000);
        dispatch_async(dispatch_get_main_queue(), ^(){
        // Create a UICollectionViewFlowlayout and assign it to the collectionView
        UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)[[self collectionView] collectionViewLayout];
        
        //Grab the first picture of list
        UIImage *firstPicture = [[[productList getItems] objectAtIndex:0] getPicture];
        
        CGFloat ratio = firstPicture.size.height/firstPicture.size.width;
        
        
        flowLayout.itemSize = CGSizeMake(screenWidth*88/186, screenWidth* (ratio * 88/186 + 0.152));//Depending on ScreenSize and the ratio of the first image
        flowLayout.minimumInteritemSpacing = (screenWidth * (2/93));//Define the horizontal space between cells
        flowLayout.minimumLineSpacing = screenWidth/28;//define the vertical spacing between cells
        flowLayout.sectionInset= UIEdgeInsetsMake(screenWidth/23,  1.5*screenWidth/93, screenWidth/93, 1.5*screenWidth/93);//effively a way to get the spacing betweeb the block of cells and header, sides and bottom
        
        //Only the ratio is needed -> a perfect case for a property and class method in ProductItem
        UIImage *headerImage = headerPicture;
        CGFloat  headerRatio = headerImage.size.height / headerImage.size.width;
        flowLayout.headerReferenceSize = CGSizeMake(screenWidth, screenWidth*headerRatio);
        
        
        [loadingCircle stopAnimating];
        [self.collectionView reloadData];
        [self loadInitialImages];
        [self loadImagesForBuffer];
        [self loadImagesForRestOfList];
        });
       
    }];
    [createLayoutOp addDependency:firstImageOp];
    [createLayoutOp addDependency:headerImageOp];
    
    //4. creates flow layout
    [queue addOperation:createLayoutOp];

    screenWidth = [UIScreen mainScreen].bounds.size.width; //Assign the screenWidth as a global variable
    
    [self.collectionView setShowsVerticalScrollIndicator:NO];//Deactivate the Vertical Scroll Indicator
    
    //Register the Custom Classes
    [self.collectionView registerClass:[CustomCollectionViewCell class] forCellWithReuseIdentifier:@"CustomCell"];  //Register Custom Cell
    
    [self.collectionView registerClass:[PLHeaderCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Header"]; //Register Custom Header
    
    
    
    
}

- (void)createButtons{
    for(Button *button in buttons){
     [self createButtonWithX: [button.x floatValue] withY: [button.y floatValue] withWidth:[button.width floatValue]  withHeight: [button.height floatValue] withType:button.type withBrand:button.brand withName:button.name withPage:button.page];
    }
}

- (void)createSwipes{
    UISwipeGestureRecognizer *swipeRightGesture=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    [self.view addGestureRecognizer:swipeRightGesture];
    swipeRightGesture.direction=UISwipeGestureRecognizerDirectionRight;
}

-(void)handleSwipeGesture:(UISwipeGestureRecognizer *) sender
{
    
    if(sender.direction==UISwipeGestureRecognizerDirectionDown){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ControlPanelNavigation *controlVC = [storyboard instantiateViewControllerWithIdentifier:@"controlPanelNavigator"];
        controlVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        ControlPanelViewController *controlPanelVC = (ControlPanelViewController *)controlVC.topViewController;
        
        if(progressIndicator!=nil){
            
            controlPanelVC.progressIndicator = progressIndicator;
            
        }
        if ([FBSDKAccessToken currentAccessToken]) {
            
            controlPanelVC.login.hidden = YES;
            
            
        }
        else{
            
            
            controlPanelVC.FBlogin.hidden = YES;
            
            AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                                                                  initWithRegionType:AWSRegionUSEast1
                                                                  identityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
            
            AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
            
            [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
     
            AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
            AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
            
            queryExpression.hashKeyAttribute = @"ID";
            queryExpression.hashKeyValues = credentialsProvider.identityId;
            
            [[dynamoDBObjectMapper query:[User class]
                              expression:queryExpression]
             continueWithBlock:^id(AWSTask *task) {
                 
                 if (task.error) {
                     NSLog(@"The request failed. Error: [%@]", task.error);
                 }
                 if (task.exception) {
                     NSLog(@"The request failed. Exception: [%@]", task.exception);
                 }
                 if (task.result) {
                     
                     AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
                     for(User *user in paginatedOutput.items){
                         if(user.Login==true||user.Login==1){
                             
                             controlPanelVC.loginText = @"Logged In";
                             
                         }
                     }
                 }
                 return nil;
             }];
            
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:controlVC animated:YES completion:nil];
        });
        
    }
    else if(sender.direction==UISwipeGestureRecognizerDirectionRight){
        if(![HistoryMemory isEmpty]){
            
            [HistoryMemory setSharedIndex: [HistoryMemory getSharedIndex]-1];
            if([[[HistoryMemory sharedCategories] objectAtIndex:[HistoryMemory getSharedIndex]] isEqualToString:@"CP"]){
                CategoricalViewController *vc = [[CategoricalViewController alloc] init];
                vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                NSString *empty = @"";
                if([[HistoryMemory sharedHistory] objectAtIndex:[HistoryMemory getSharedIndex]]!=nil){
                    vc.progressIndicator = [empty stringByAppendingString: [[HistoryMemory sharedHistory]objectAtIndex:[HistoryMemory getSharedIndex]]];
                }
                [self presentViewController:vc animated:YES completion:nil];
            }
            else if([[[HistoryMemory sharedCategories] objectAtIndex:[HistoryMemory getSharedIndex]] isEqualToString:@"PL"]){
                ListCollectionViewController *vc = [[ListCollectionViewController alloc] init];
                vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                NSString *empty = @"";
                if([[HistoryMemory sharedHistory] objectAtIndex:[HistoryMemory getSharedIndex]]!=nil){
                    vc.progressIndicator = [empty stringByAppendingString: [[HistoryMemory sharedHistory]objectAtIndex:[HistoryMemory getSharedIndex]]];
                }
                [self presentViewController:vc animated:YES completion:nil];
            }
            
        }
    }
   
}

- (void)didReceiveMemoryWarning {
    [self.pendingOperations.downloadQueue cancelAllOperations];
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    // We only show one section, so this return-value can be hardcoded
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return [[productList getItems] count];
    
    
}

//Used for header
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    UIImage *headerImage = headerPicture;//Get the header from the datamodel
    CGFloat  headerRatio = headerImage.size.height / headerImage.size.width; //Calculate the ratio to make imageView perfect for the headerImage
    
    
   
    if (kind == UICollectionElementKindSectionHeader){ //Check to see if it really is a header
        
        PLHeaderCollectionReusableView *reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Header" forIndexPath:indexPath];
        
        if (reusableView == nil){
             reusableView = [[PLHeaderCollectionReusableView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenWidth*headerRatio)];
        }
        
        reusableView.headerImageView.frame = CGRectMake(0, 0, screenWidth, screenWidth*headerRatio);
        [reusableView.headerImageView setContentMode:UIViewContentModeScaleAspectFill];  //The ContentMode makes sure the Aspecttratio of the displaved image is not changed
        reusableView.headerImageView.image = headerImage;
        
        return reusableView;
        
    }
    
    
    return nil;
};

//Returns the visible cell object at the specified index path
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    [[cell contentView] setFrame:[cell bounds]];
    [cell setBackgroundColor:[UIColor whiteColor]];
    
    CGFloat cellWidth = cell.frame.size.width;
    ProductItem *currentItem = [productList getItems][indexPath.row];

    if ([currentItem hasImage]) {
      
        cell.imageView.image = [currentItem getPicture]; //Set ImageView image
        cell.nameLabel.text = [currentItem getProduct_name];
        
    }
    else if([currentItem isFailed]){
       
        cell.imageView.image = [UIImage imageNamed:@"Failed.png"];
        cell.nameLabel.text = @"Failed to load";
      
        
    }
    else{
        BOOL indicatorAlreadyPresent = false;
        for (NSString *key in indicatorDictionary){
            if([key intValue] ==(int)indexPath.row){
                indicatorAlreadyPresent = true;
            }
        }
        
        //Only adds ActivtyIndicator when one is not already present
        if(!indicatorAlreadyPresent){
           UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
           [activityIndicator setFrame:CGRectMake((cell.frame.size.width/2)-20,(cell.frame.size.height/2)-20,40,40)];
           [[cell contentView] addSubview:activityIndicator];
           [activityIndicator setHidesWhenStopped:YES];
           [activityIndicator startAnimating];
           [indicatorDictionary setObject:activityIndicator  forKey:[NSString stringWithFormat:@"%d",(int)indexPath.row]];
      
        }
        cell.imageView.image = [UIImage imageNamed:@"Placeholder.png"];
        cell.nameLabel.text = [currentItem getProduct_name];
     
        if (!collectionView.dragging && !collectionView.decelerating) {
       
            [self startImageDownloadingForItem:currentItem atIndexPath:indexPath withPriority:NSOperationQueuePriorityHigh andQuality:NSQualityOfServiceUserInteractive];
        }
        
    }
    
    //-----------------ImageView-----------------------
    
    CGFloat ratio = cell.imageView.image.size.height/cell.imageView.image.size.width; //Calculate the ratio (height/width) of the image

    cell.imageView.frame = CGRectMake(0, 0, cellWidth, cellWidth*ratio);//Customfit the imageView using the ratio
    
    [cell.imageView setContentMode:UIViewContentModeScaleAspectFill]; //Define UIContent Mode to make sure the image is not distorted
    
    //-----------------Label for the Name-----------------------
    
    CGFloat namefontSize = 15; //font from the database
    CGFloat screenSizeCorrection = cellWidth/170;//Take screenSize into accout
    cell.nameLabel.font = [UIFont fontWithName:@"Helvetica Neue" size: namefontSize*screenSizeCorrection];
    
    cell.nameLabel.lineBreakMode = NSLineBreakByWordWrapping;//hardcoded (uniform across the platform)
    cell.nameLabel.textAlignment = NSTextAlignmentCenter; //hardcoded (uniform across the platform)
    cell.nameLabel.numberOfLines =0; //This way he number can adjust to the intrinsic ContentSize
    cell.nameLabel.textColor = [UIColor blackColor];//Set the TextColor
    
    //>>>>>>>>Constraints on the nameLabel<<<<<<<<<<<<<
    
    //Deactivate Autotranslate
    cell.nameLabel.translatesAutoresizingMaskIntoConstraints=NO;
    
    CGFloat textTopSpace = cellWidth/56; // Define a Variable for the space from text to the ImageView
    
    //Top constraint (using textTopSpace)
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cell.nameLabel attribute:(NSLayoutAttributeTop) relatedBy:(NSLayoutRelationEqual) toItem:cell.imageView attribute:(NSLayoutAttributeBottom) multiplier:1 constant: textTopSpace]];

    CGFloat textInset = cellWidth/83;//Define a variable to define the inset of the text (the min space between text and cell)
    
    //Left Constrain
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cell.nameLabel attribute:(NSLayoutAttributeLeading) relatedBy:(NSLayoutRelationEqual) toItem:cell.contentView attribute:(NSLayoutAttributeLeading) multiplier:1 constant:textInset]];
    
    //Right Constrain
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cell.nameLabel attribute:(NSLayoutAttributeTrailing) relatedBy:(NSLayoutRelationEqual) toItem:cell.contentView attribute:(NSLayoutAttributeTrailing) multiplier:1 constant:(-1) * textInset]];//Use (-1) * textInset because the model interprets a possitive x-value to go from left to the right
    
    //----------------Label for the Price---------------
 
    NSString *price = @"56.98";//Get the price for the database
    cell.priceLabel.text = [NSString stringWithFormat:@"$ %@", price];
    
    CGFloat priceFontSize = 12.3;//from the data model
    cell.priceLabel.font = [UIFont fontWithName:@"Avenir" size:priceFontSize*screenSizeCorrection];
    
    cell.priceLabel.textAlignment = NSTextAlignmentCenter; //hardcoded (uniform across the platform)
    cell.priceLabel.numberOfLines =1;
    cell.priceLabel.textColor = [UIColor blackColor];

    //>>>>>Constrain<<<<<<<
    //Deactivate Autotranslate
     cell.priceLabel.translatesAutoresizingMaskIntoConstraints= NO;
    
    CGFloat priceTopDistance = cellWidth/234;//Top space to the nameLabel
    
    //Top Constrain to the nameLabel
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cell.priceLabel attribute:(NSLayoutAttributeTop) relatedBy:(NSLayoutRelationEqual) toItem:cell.nameLabel attribute:(NSLayoutAttributeBottom) multiplier:1 constant:priceTopDistance]];
    
    //Left Constraint
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cell.priceLabel attribute:(NSLayoutAttributeLeading) relatedBy:(NSLayoutRelationEqual) toItem:cell.contentView attribute:(NSLayoutAttributeLeading) multiplier:1 constant:0]];
    
    //Right Contraint
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cell.priceLabel attribute:(NSLayoutAttributeTrailing) relatedBy:(NSLayoutRelationEqual) toItem:cell.contentView attribute:(NSLayoutAttributeTrailing) multiplier:1 constant:0]];
    
    //Cell Appearance
    //Round the Edges
    cell.contentView.layer.cornerRadius = 5;
    cell.contentView.layer.masksToBounds = true;
    
    //Set the backgroundcolor of the Cell
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = UIColorFromRGB(0xC0B6A1);

    return cell;
}

- (void)startImageDownloadingForItem:(ProductItem *)item atIndexPath:(NSIndexPath *)indexPath withPriority:(NSOperationQueuePriority)priority andQuality:(NSQualityOfService)quality{

   if (!item.hasImage) {
    // 1: First, check for the particular indexPath to see if there is already an operation in downloadsInProgress for it. If so, ignore it.
    if (![self.pendingOperations.downloadsInProgress.allKeys containsObject:indexPath]) {
        
        // 2: If not, create an instance of ImageDownloader by using the designated initializer, and set ListCollectionViewController as the delegate. Pass in the appropriate indexPath and a pointer to the instance of ProductItem, and then add it to the download queue. You also add it to downloadsInProgress to help keep track of things.
        // Start downloading
        ImageDownloader *imageDownloader = [[ImageDownloader alloc] initWithProductItem:item atIndexPath:indexPath delegate:self];
        [imageDownloader setQueuePriority:priority];
        [imageDownloader setQualityOfService:quality];
        [self.pendingOperations.downloadsInProgress setObject:imageDownloader forKey:indexPath];
        [self.pendingOperations.downloadQueue addOperation:imageDownloader];
    }
   }

}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    Button *sender = [buttons objectAtIndex:indexPath.row];
    ProductViewController *productVC = [[ProductViewController alloc] init];
    productVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    
    if(![HistoryMemory isEmpty]){
        [HistoryMemory setSharedIndex:[HistoryMemory getSharedIndex]+1];
    }
    
    for(ProductItem *item in [productList getItems]){
        if([[item getProduct_name] isEqualToString:sender.name]){
            productVC.item = [[ProductItem alloc] init];
            [productVC.item setProduct_name:[item getProduct_name]];
            /* [productVC.item setProduct_description:[item getProduct_description]];
             [productVC.item setProduct_id:[item getProduct_id]];
             [productVC.item setProduct_brand_id:[item getProduct_brand_id]];
             [productVC.item setProduct_available_quantity:[item getProduct_available_quantity]];
             [productVC.item setPrice:[item getPrice]];*/
            [productVC.item setPicture:[item getPicture]];
        }
    }
    
    [self presentViewController:productVC animated:YES completion:nil];
}

#pragma mark -
#pragma mark - ImageDownloader delegate

- (void)imageDownloaderDidFinish:(ImageDownloader *)downloader {
    
    // 1: Check for the indexPath of the operation.
    NSIndexPath *indexPath = downloader.indexPathInCollectionView;
    
    // 2: Get hold of the ProductItem instance.
    ProductItem *item = downloader.productItem;
    
    // 3: Replace the updated ProductItem in the main data source (Product List).
    [[productList getItems] replaceObjectAtIndex:indexPath.row withObject:item];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //Remove activityIndicator
      for (NSString *key in indicatorDictionary){
        
        //If more than one section is present, one needs to add row property
        if( [key intValue]==(int)indexPath.row){
            dispatch_async(dispatch_get_main_queue(), ^(void){
              [[indicatorDictionary objectForKey:key] stopAnimating];
              [[indicatorDictionary objectForKey:key] removeFromSuperview];
            });
        }
      }
    });
    
    // 4: Update UI.
    [self.collectionView reloadItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
   
    // 5: Remove the operation from downloadsInProgress.
    [self.pendingOperations.downloadsInProgress removeObjectForKey:indexPath];
}

#pragma mark - UIScrollView delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    // 1: As soon as the user starts scrolling, you will want to suspend all operations and take a look at what the user wants to see.
    [self.pendingOperations.downloadQueue setSuspended:YES];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    //true if still moving
   // if (decelerate) {
        [self loadImagesForOnscreenCells];
        
        //We'll load buffer images after the onscreen images
        [self loadImagesForBuffer];
        [self.pendingOperations.downloadQueue setSuspended:NO];
    //}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // 3: This delegate method tells you that collection view stopped scrolling, so you will do the same as in #2.
    [self loadImagesForOnscreenCells];
    
    //We'll load buffer images after the onscreen images
    [self loadImagesForBuffer];
    
    //We'll also load the rest of the images. That download has a low priority though
    [self loadImagesForRestOfList];
    [self.pendingOperations.downloadQueue setSuspended:NO];
}

- (void)loadInitialImages {
    // 1: Get a set of visible rows.
   
    NSSet *visibleCells = [NSSet setWithArray:[self.collectionView indexPathsForVisibleItems]];
    
    if([visibleCells count]==0){
        int start = 0;
        int end = 3;
        if(!((historyIndexPathMin==nil)||(historyIndexPathMax==nil))){
            start = (int)historyIndexPathMin.row;
            end = (int)historyIndexPathMax.row;
        }
        for(int i = start;i<=end;i++){
            ProductItem *itemToProcess = [[productList getItems] objectAtIndex:i];
            [self startImageDownloadingForItem:itemToProcess atIndexPath:[NSIndexPath indexPathForRow:i inSection:0] withPriority:NSOperationQueuePriorityVeryHigh andQuality:NSQualityOfServiceUserInteractive];
        }
        return;
    }
    NSMutableSet *toBeStarted = [visibleCells mutableCopy];
    
    for (NSIndexPath *anIndexPath in toBeStarted) {
      
        ProductItem *itemToProcess = [[productList getItems] objectAtIndex:anIndexPath.row];
        [self startImageDownloadingForItem:itemToProcess atIndexPath:anIndexPath withPriority:NSOperationQueuePriorityVeryHigh andQuality:NSQualityOfServiceUserInteractive];
        
    }
    toBeStarted = nil;
}

- (void)loadImagesForOnscreenCells {
    
    // 1: Get a set of visible rows.
    NSSet *visibleCells = [NSSet setWithArray:[self.collectionView indexPathsForVisibleItems]];
    
    // 2: Get a set of all pending operations (download).
    NSMutableSet *pendingOperations = [NSMutableSet setWithArray:[self.pendingOperations.downloadsInProgress allKeys]];
    
    NSMutableSet *toBeCancelled = [pendingOperations mutableCopy];
    NSMutableSet *toBeStarted = [visibleCells mutableCopy];
    
    // 3: Rows (or indexPaths) that need an operation = visible rows and pendings.
    [toBeStarted minusSet:pendingOperations];
    
    // 4: Rows (or indexPaths) that their operations should be cancelled = pendings and visible rows.
    [toBeCancelled minusSet:visibleCells];
    
    // 5: Loop through those to be cancelled, cancel them, and remove their reference from PendingOperations.
      for (NSIndexPath *anIndexPath in toBeCancelled) {
        
        ImageDownloader *pendingDownload = [self.pendingOperations.downloadsInProgress objectForKey:anIndexPath];
        
        //Beware of possible race conditions between isExecuting and cancel
        if(![pendingDownload isExecuting]){
           [pendingDownload cancel];
           [self.pendingOperations.downloadsInProgress removeObjectForKey:anIndexPath];
        }
      }
    
    toBeCancelled = nil;
    
    // 6: Loop through those to be started, and call startOperationsForProductItem:atIndexPath: for each.
    for (NSIndexPath *anIndexPath in toBeStarted) {
    
        ProductItem *itemToProcess = [[productList getItems] objectAtIndex:anIndexPath.row];
        [self startImageDownloadingForItem:itemToProcess atIndexPath:anIndexPath withPriority:NSOperationQueuePriorityHigh andQuality:NSQualityOfServiceUserInteractive];
        
    }
    toBeStarted = nil;
    
}

- (void)loadImagesForBuffer {

    // 1: Get a set of visible rows.
    NSSet *visibleCells = [NSSet setWithArray:[self.collectionView indexPathsForVisibleItems]];
    int minRow = 0;
    int maxRow = 0;
    for(NSIndexPath *indexPath in visibleCells){
        if((int)indexPath.row<minRow){
            minRow = (int)indexPath.row;
        }
        else if((int)indexPath.row>maxRow){
            maxRow = (int)indexPath.row;
        }
    }
   
    //used for startup when no images have yet been loaded
    if(minRow==maxRow){
        if((historyIndexPathMax==nil)||(historyIndexPathMin==nil)){
           maxRow = 3;
        }
        else{
            minRow = (int)historyIndexPathMin.row;
            maxRow = (int)historyIndexPathMax.row;
        }
    }
    NSMutableSet *buffer = [[NSMutableSet alloc] init];
    //want to put 4 images on each side in buffer
    for(int i = (maxRow+1);i<=(maxRow+4);i++){
        if(i<[[productList getItems] count]){
            [buffer addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        }
    }
    
    //want to put 4 images on each side in buffer
    for(int i = (minRow-1);i>=(minRow-4);i--){
        if(i>=0){
            [buffer addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        }
    }
  
    
    // 2: Get a set of all pending operations (download).
    NSMutableSet *pendingOperations = [NSMutableSet setWithArray:[self.pendingOperations.downloadsInProgress allKeys]];
    
    NSMutableSet *toBeStarted = [buffer mutableCopy];
    
    // 3: Rows (or indexPaths) that need an operation = buffer rows and pendings.
    [toBeStarted minusSet:pendingOperations];
    
    // 6: Loop through those to be started, and call startOperationsForPhotoRecord:atIndexPath: for each.
    for (NSIndexPath *anIndexPath in toBeStarted) {
       
        ProductItem *itemToProcess = [[productList getItems] objectAtIndex:anIndexPath.row];
        [self startImageDownloadingForItem:itemToProcess atIndexPath:anIndexPath withPriority:NSOperationQueuePriorityNormal andQuality:NSQualityOfServiceUserInitiated];
        
    }
    toBeStarted = nil;
    
}

- (void)loadImagesForRestOfList {
    // 1: Get a set of visible rows.
    NSSet *visibleCells = [NSSet setWithArray:[self.collectionView indexPathsForVisibleItems]];
    int minRow = 0;
    int maxRow = 0;
    for(NSIndexPath *indexPath in visibleCells){
        if((int)indexPath.row<minRow){
            minRow = (int)indexPath.row;
        }
        else if((int)indexPath.row>maxRow){
            maxRow = (int)indexPath.row;
        }
    }
    
    //used for startup when no images have yet been loaded
    if(minRow==maxRow){
        if((historyIndexPathMax==nil)||(historyIndexPathMin==nil)){
            maxRow = 3;
        }
        else{
            minRow = (int)historyIndexPathMin.row;
            maxRow = (int)historyIndexPathMax.row;
        }
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
      for(int i = (maxRow+1);i<[[productList getItems] count];i++){
        if(![self.pendingOperations.downloadsInProgress.allKeys containsObject:[NSIndexPath indexPathForRow:i inSection:0]]){
            ProductItem *itemToProcess = [[productList getItems] objectAtIndex:i];
            [self startImageDownloadingForItem:itemToProcess atIndexPath:[NSIndexPath indexPathForRow:i inSection:0] withPriority:NSOperationQueuePriorityLow andQuality:NSQualityOfServiceUserInitiated];
        }
      }
    });
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
      for(int i = (minRow-1);i>=0;i--){
        if(![self.pendingOperations.downloadsInProgress.allKeys containsObject:[NSIndexPath indexPathForRow:i inSection:0]]){
            ProductItem *itemToProcess = [[productList getItems] objectAtIndex:i];
            [self startImageDownloadingForItem:itemToProcess atIndexPath:[NSIndexPath indexPathForRow:i inSection:0] withPriority:NSOperationQueuePriorityLow andQuality:NSQualityOfServiceUserInitiated];
        }
      }
    });
    
    
}

- (void)createButtonWithX:(CGFloat)x withY:(CGFloat)y withWidth:(CGFloat)width withHeight:(CGFloat)height withType:(NSString *)type withBrand:(NSString *)brand withName:(NSString *)name withPage:(NSString *)page{
    
    SpecialButton *button = [SpecialButton buttonWithType:UIButtonTypeRoundedRect];
    
    button.x = @(x);
    button.y = @(y);
    button.width = @(width);
    button.height = @(height);
    button.type = type;
    button.brand = brand;
    button.name = name;
    button.page = page;
    [button addTarget:self
               action:@selector(btnTouched:)
     forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(x, y, width, height);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view addSubview:button];
    });
}



@end

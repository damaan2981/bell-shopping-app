//
//  ImageDownloader.h
//  App
//
//  Created by Internet Wu on 13/11/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import <Foundation/Foundation.h>
// 1
#import "ProductItem.h"

// 2
@protocol ImageDownloaderDelegate;
@interface ImageDownloader : NSOperation
@property (nonatomic, assign) id <ImageDownloaderDelegate> delegate;

// 3
@property (nonatomic, readonly, strong) NSIndexPath *indexPathInCollectionView;
@property (nonatomic, readonly, strong) ProductItem *productItem;

// 4
- (id)initWithProductItem:(ProductItem *)item atIndexPath:(NSIndexPath *)indexPath delegate:(id<ImageDownloaderDelegate>) theDelegate;

@end

@protocol ImageDownloaderDelegate <NSObject>

// 5
- (void)imageDownloaderDidFinish:(ImageDownloader *)downloader;
@end

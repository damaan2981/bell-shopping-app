//
//  Crypto.h
//  App
//
//  Created by Internet Wu on 25/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Crypto : NSObject

+(NSData *)decrypt:(NSString *)data key:(NSString *)key;
+(NSData *)aes128Decrypt:(NSData *)data key:(NSData *)key withIV:(NSData *)iv;

+(NSData *)hexDecode:(NSString *)hexString;
+(NSString *)hexEncode:(NSString *)string;

+(NSData *)sha256HMac:(NSData *)data withKey:(NSString *)key;

+(NSString *)generateRandomString;

@end

//
//  RegistrationViewController.h
//  App
//
//  Created by Internet Wu on 03/09/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import <Foundation/Foundation.h>
#import "User.h"
#import "AWSIdentityManager.h"
#import "ControlPanelNavigation.h"
#import "AWSCognitoIdentityModel.h"
#import "AWSCognitoIdentityService.h"
@interface RegistrationViewController : UIViewController
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
@property (nonatomic, strong) NSString *progressIndicator;
@end


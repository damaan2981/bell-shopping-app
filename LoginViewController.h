//
//  LoginViewController.h
//  App
//
//  Created by Internet Wu on 03/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import <Foundation/Foundation.h>
#import "User.h"
#import "AWSIdentityManager.h"
#import "ControlPanelNavigation.h"
#import "RegistrationViewController.h"

@interface LoginViewController : UIViewController
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
@property NSString* FBID;
@property (nonatomic, strong) NSString* progressIndicator;

@end

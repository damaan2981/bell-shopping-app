//
//  Button.m
//  App
//
//  Created by Internet Wu on 13/09/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

#import "Button.h"

@implementation Button

+ (NSString *)dynamoDBTableName {
    return @"Buttons";
}

+ (NSString *)hashKeyAttribute {
    return @"name";
}

+ (NSString *)rangeKeyAttribute {
    return @"page";
}

@end

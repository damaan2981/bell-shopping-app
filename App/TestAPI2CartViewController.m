//
//  TestAPI2CartViewController.m
//  App
//
//  Created by Internet Wu on 29/08/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

#import "TestAPI2CartViewController.h"
#define kAPIKEY @"327a2daf2a004fc37089da93b8a5089f"
#define kSTOREKEY @"6954bf6f57a79244e6a759b1bf185ef0"
#import <RestKit/RestKit.h>
#import "Product.h"

@interface TestAPI2CartViewController ()

@end

@implementation TestAPI2CartViewController

NSArray *products;

- (void)viewDidLoad {
    [super viewDidLoad];
    

    [self loadProducts];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)loadProducts
{

    //this should reflect the JSON response parameters from the API
    RKObjectMapping* productMapping = [RKObjectMapping mappingForClass:[Product class]];
    [productMapping addAttributeMappingsFromDictionary:@{
                                                         @"id": @"id",
                                                         @"name": @"name",
                                                         @"description": @"desc",
                                                         @"price": @"price",
                                                         @"categories_ids": @"categories_ids"
                                                         }];

    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping: productMapping method:RKRequestMethodAny pathPattern:nil keyPath:@"result.product" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    //replace later on with a dynamic value
    NSURL *URL = [NSURL URLWithString:@"https://api.api2cart.com/v1.0/product.list.json?api_key=327a2daf2a004fc37089da93b8a5089f&store_key=6954bf6f57a79244e6a759b1bf185ef0"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[ responseDescriptor ]];
    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
       
        NSArray* formatted = [[NSArray alloc] initWithArray:mappingResult.array];
        for(Product *product in formatted){
            NSLog(@"%@",[self description:product]);
        }
       
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        RKLogError(@"Operation failed with error: %@", error);
    }];
    
    [objectRequestOperation start];
 
}

- (NSString *)description:(Product*)product{
    return [NSString stringWithFormat: @"Product: id=%@ name=%@ desc=%@ price=%@ categories_ids=%@", product.id, product.name, product.desc, product.price, product.categories_ids];
}

@end

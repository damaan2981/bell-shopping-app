//
//  main.m
//  App
//
//  Created by Internet Wu on 29/08/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

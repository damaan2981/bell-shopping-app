//
//  ControlPanelViewController.m
//  App
//
//  Created by Internet Wu on 05/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import "ControlPanelViewController.h"

@interface ControlPanelViewController ()

@end

@implementation ControlPanelViewController
@synthesize progressIndicator;
@synthesize login;
@synthesize FBlogin;
@synthesize FBID;
@synthesize loginText;

- (void)viewDidLoad {
    [super viewDidLoad];
 
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                                                          initWithRegionType:AWSRegionUSEast1
                                                          identityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];

    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
    
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
    
    //If we cleared the Keychain during logout, a new identityId is requested here
    [[credentialsProvider getIdentityId] continueWithBlock:^id(AWSTask *task){
        NSLog(@"%@",credentialsProvider.identityId);
        return nil;
    }];

    
    
    [FBlogin setDelegate:self];
    [login addTarget:self
               action:@selector(clicked:)
     forControlEvents:UIControlEventTouchUpInside];
 
    if (![FBSDKAccessToken currentAccessToken]) {
        FBlogin.hidden = YES;
        if(loginText!=nil){
             [login setTitle: @"Log Out" forState: UIControlStateNormal];
        }
        
    }
    else{
        FBID = [FBSDKAccessToken currentAccessToken].userID;
        login.hidden = YES;
    }
    UISwipeGestureRecognizer *swipeUpGesture=[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    [self.view addGestureRecognizer:swipeUpGesture];
    swipeUpGesture.direction=UISwipeGestureRecognizerDirectionUp;

   
}

-(void)handleSwipeGesture:(UISwipeGestureRecognizer *) sender
{
    
    if(sender.direction==UISwipeGestureRecognizerDirectionUp){
        CategoricalViewController *categoricalVC = [[CategoricalViewController alloc] init];
        categoricalVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        NSString *empty = @"";
        if(progressIndicator!=nil){
            categoricalVC.progressIndicator = [empty stringByAppendingString: progressIndicator];
            
        }
        
        [self presentViewController:categoricalVC animated:YES completion:nil];
        
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)goHomepage:(id)sender {
    CategoricalViewController *categoricalVC = [[CategoricalViewController alloc] init];
    categoricalVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:categoricalVC animated:YES completion:nil];
}

//This return to the Brand Homepage
- (IBAction)goBrand:(id)sender {
    NSArray *progress = [progressIndicator componentsSeparatedByString:@"/"];
    if([progress count]>=2){
        CategoricalViewController *categoricalVC = [[CategoricalViewController alloc] init];
        categoricalVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        categoricalVC.progressIndicator = @"Brands/";
        categoricalVC.progressIndicator = [categoricalVC.progressIndicator stringByAppendingString:[progress objectAtIndex:1]];
        categoricalVC.progressIndicator = [categoricalVC.progressIndicator stringByAppendingString:@"/"];
        categoricalVC.passedButton = [[Button alloc] init];
        //This is set just for the purpose of creating buttons, as acceptable values are only CP and PL
        categoricalVC.passedButton.type = @"CP";
        categoricalVC.passedButton.brand = @"NULL";
        categoricalVC.passedButton.name = [progress objectAtIndex:1];
        [self presentViewController:categoricalVC animated:YES completion:nil];
    }
    else{
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Not Available!"
                                      message:@"You are not currently in any brands' section!"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];

    }
}

-(void)clicked:(UIButton*)sender{
   
    //executes when user logs out from as a developer authenticated identity
    if([login.titleLabel.text isEqualToString:@"Log Out"]){
      
        
        AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                                                              initWithRegionType:AWSRegionUSEast1
                                                              identityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
        
        AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
        
        [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
        
        AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
        AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
        
        queryExpression.hashKeyAttribute = @"ID";
        queryExpression.hashKeyValues = credentialsProvider.identityId;
        
        [[dynamoDBObjectMapper query:[User class]
                          expression:queryExpression]
         continueWithBlock:^id(AWSTask *task) {
             
             if (task.error) {
                 NSLog(@"The request failed. Error: [%@]", task.error);
             }
             if (task.exception) {
                 NSLog(@"The request failed. Exception: [%@]", task.exception);
             }
             if (task.result) {
                 
                 AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
                 for(User *user in paginatedOutput.items){
                     user.Login = false;
                     [[dynamoDBObjectMapper save:user]
                      continueWithBlock:^id(AWSTask *task) {
                          if (task.error) {
                              NSLog(@"The request failed. Error: [%@]", task.error);
                          }
                          if (task.exception) {
                              NSLog(@"The request failed. Exception: [%@]", task.exception);
                          }
                          if (task.result) {
                              //Do something with the result.
                              
                          }
                          return nil;
                      }];
                     
                 }
             }
             return nil;
         }];
        
        
        [credentialsProvider clearKeychain];
    
        
       
        
       
         /*   AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
            AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
            
            queryExpression.hashKeyAttribute = @"ID";
            queryExpression.hashKeyValues = credentialsProvider.identityId;
            
            [[dynamoDBObjectMapper query:[User class]
                              expression:queryExpression]
             continueWithBlock:^id(AWSTask *task) {
                 
                 if (task.error) {
                     NSLog(@"The request failed. Error: [%@]", task.error);
                 }
                 if (task.exception) {
                     NSLog(@"The request failed. Exception: [%@]", task.exception);
                 }
                 if (task.result) {
                     
                     AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
                     if([paginatedOutput.items count]==0){
                         User *user = [[User alloc] init];
                         user.ID = credentialsProvider.identityId;
                         user.Email = @"NULL";
                         user.Password = @"NULL";
                         user.FirstName = @"NULL";
                         user.LastName = @"NULL";
                         user.Login = false;
                         user.FBID = @"NULL";
                         [[dynamoDBObjectMapper save:user]
                          continueWithBlock:^id(AWSTask *task) {
                              if (task.error) {
                                  NSLog(@"The request failed. Error: [%@]", task.error);
                              }
                              if (task.exception) {
                                  NSLog(@"The request failed. Exception: [%@]", task.exception);
                              }
                              if (task.result) {
                                  //Do something with the result.
                              }
                              return nil;
                          }];
                     }
                 }
                 return nil;
             }];*/
     
        
        
        
        
        
        
        
        
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ControlPanelNavigation *controlVC = [storyboard instantiateViewControllerWithIdentifier:@"controlPanelNavigator"];
        controlVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        ControlPanelViewController *controlPanelVC = (ControlPanelViewController *)controlVC.topViewController;
        if(progressIndicator!=nil){
            
            controlPanelVC.progressIndicator = progressIndicator;
            
        }
        controlPanelVC.FBlogin.hidden = YES;
     
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [self presentViewController:controlVC animated:YES completion:nil];
        });
    }
    else{
       [self performSegueWithIdentifier:@"showLogin" sender:sender];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showLogin"]) {
        
        // Get reference to the destination view controller
        LoginViewController *vc = [segue destinationViewController];
        
        if(progressIndicator!=nil){
            
            vc.progressIndicator = progressIndicator;
            
        }
    }}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                                                          initWithRegionType:AWSRegionUSEast1
                                                          identityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
    
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
    NSLog(@"%@",credentialsProvider.identityId);
    AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
    queryExpression.indexName = @"FBID-index";
    queryExpression.hashKeyAttribute = @"FBID";
    queryExpression.hashKeyValues = FBID;
    
    [[dynamoDBObjectMapper query:[User class]
                      expression:queryExpression]
     continueWithBlock:^id(AWSTask *task) {
         
         if (task.error) {
             NSLog(@"The request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             for(User *user in paginatedOutput.items){
                 user.Login = false;
                 [[dynamoDBObjectMapper save:user]
                  continueWithBlock:^id(AWSTask *task) {
                      if (task.error) {
                          NSLog(@"The request failed. Error: [%@]", task.error);
                      }
                      if (task.exception) {
                          NSLog(@"The request failed. Exception: [%@]", task.exception);
                      }
                      if (task.result) {
                          //Do something with the result.
                      }
                      return nil;
                  }];
                 
             }
             
         }
         return nil;
     }];
  
    [self handleLogout];
   
    
}

- (void)handleLogout {
    
    if (![FBSDKAccessToken currentAccessToken]) {
        
        [[AWSIdentityManager sharedInstance] logoutWithCompletionHandler:^(id result, NSError *error) {
            AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                                                                  initWithRegionType:AWSRegionUSEast1
                                                                  identityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
            
            AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
            
            [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
         
            AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
            AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
            
            queryExpression.hashKeyAttribute = @"ID";
            queryExpression.hashKeyValues = credentialsProvider.identityId;
            
            [[dynamoDBObjectMapper query:[User class]
                              expression:queryExpression]
             continueWithBlock:^id(AWSTask *task) {
                 
                 if (task.error) {
                     NSLog(@"The request failed. Error: [%@]", task.error);
                 }
                 if (task.exception) {
                     NSLog(@"The request failed. Exception: [%@]", task.exception);
                 }
                 if (task.result) {
                     
                     AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
                     if([paginatedOutput.items count]==0){
                         User *user = [[User alloc] init];
                         user.ID = credentialsProvider.identityId;
                         user.Email = @"NULL";
                         user.Password = @"NULL";
                         user.FirstName = @"NULL";
                         user.LastName = @"NULL";
                         user.Login = false;
                         user.FBID = @"NULL";
                         [[dynamoDBObjectMapper save:user]
                          continueWithBlock:^id(AWSTask *task) {
                              if (task.error) {
                                  NSLog(@"The request failed. Error: [%@]", task.error);
                              }
                              if (task.exception) {
                                  NSLog(@"The request failed. Exception: [%@]", task.exception);
                              }
                              if (task.result) {
                                  //Do something with the result.
                              }
                              return nil;
                          }];
                     }
                 }
                 return nil;
             }];
        }];
     
    }
   
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ControlPanelNavigation *controlVC = [storyboard instantiateViewControllerWithIdentifier:@"controlPanelNavigator"];
    controlVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    ControlPanelViewController *controlPanelVC = (ControlPanelViewController *)controlVC.topViewController;
    if(progressIndicator!=nil){
        
        controlPanelVC.progressIndicator = progressIndicator;
        
    }
  
    controlPanelVC.FBlogin.hidden = YES;
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [self presentViewController:controlVC animated:YES completion:nil];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

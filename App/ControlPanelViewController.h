//
//  ControlPanelViewController.h
//  App
//
//  Created by Internet Wu on 05/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AWSS3/AWSS3.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import "Button.h"
#import "SpecialButton.h"
#import "ListCollectionViewController.h"
#import "CategoricalViewController.h"
#import "User.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "FBSDKLoginKit/FBSDKLoginKit.h"
#import "ControlPanelNavigation.h"
#import "AWSIdentityManager.h"
#import "LoginViewController.h"
@interface ControlPanelViewController : UIViewController
@property (nonatomic, strong) NSString *progressIndicator;
@property (weak, nonatomic) IBOutlet UIButton *login;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *FBlogin;
@property (nonatomic, strong) NSString *FBID;
@property (nonatomic, strong) NSString *loginText;

@end

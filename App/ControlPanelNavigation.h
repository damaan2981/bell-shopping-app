//
//  ControlPanelNavigation.h
//  App
//
//  Created by Internet Wu on 07/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ControlPanelViewController.h"
@interface ControlPanelNavigation : UINavigationController

@end

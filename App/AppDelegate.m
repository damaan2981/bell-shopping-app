//
//  AppDelegate.m
//  App
//
//  Created by Internet Wu on 29/08/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AppDelegate.h"
#import <AWSCognito/AWSCognito.h>
#import <AWSCore/AWSCore.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import <RestKit/RestKit.h>
#import "Stripe.h"
#import "Branch.h"
@interface AppDelegate ()

@end

//replace later with the live publishable key
NSString * const StripePublishableKey = @"pk_test_Mx8M8O19DOGWXlOoJf2ZHaTu";

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self getFreeDiskspace];
    //Want to clean the Library/Caches Folder on disk
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDirectory = [paths objectAtIndex:0];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //This will remove all files in the Caches Directory
    [fileManager removeItemAtPath:cachesDirectory error:nil];
    for (NSString *file in [fileManager contentsOfDirectoryAtPath:cachesDirectory error:nil]) {
        BOOL success = [fileManager removeItemAtPath:[[cachesDirectory stringByAppendingString:@"/"] stringByAppendingString:file] error:nil];
        NSLog(@"%@",[[cachesDirectory stringByAppendingString:@"/"] stringByAppendingString:file]);
        if (!success) {
            // it failed.
        }
    }
    
    //Branch Integration
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        // params are the deep linked params associated with the link that the user clicked before showing up.
       // NSLog(@"deep link data: %@", [params description]);
    }];
    
    //Stripe Integration
    [Stripe setDefaultPublishableKey:StripePublishableKey];
    
    // Initialize the Amazon Cognito credentials provider
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                                                          initWithRegionType:AWSRegionUSEast1
                                                          identityPoolId:@"us-east-1:8d20e314-c327-4d0e-8d13-6e853c27c98d"];
   
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
    
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
    //[credentialsProvider clearKeychain];
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];

        queryExpression.hashKeyAttribute = @"ID";
        queryExpression.hashKeyValues = credentialsProvider.identityId;
    
    [[dynamoDBObjectMapper query:[User class]
                      expression:queryExpression]
     continueWithBlock:^id(AWSTask *task) {
         
         if (task.error) {
             NSLog(@"The request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
         
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             if([paginatedOutput.items count]==0){
                 User *user = [[User alloc] init];
                 user.ID = credentialsProvider.identityId;
                 user.Email = @"NULL";
                 user.Password = @"NULL";
                 user.FirstName = @"NULL";
                 user.LastName = @"NULL";
                 user.Login = false;
                 user.FBID = @"NULL";
                 [[dynamoDBObjectMapper save:user]
                  continueWithBlock:^id(AWSTask *task) {
                      if (task.error) {
                          NSLog(@"The request failed. Error: [%@]", task.error);
                      }
                      if (task.exception) {
                          NSLog(@"The request failed. Exception: [%@]", task.exception);
                      }
                      if (task.result) {
                          //Do something with the result.
                      }
                      return nil;
                  }];
             }
         }
         return nil;
     }];

   
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

//When you quit the app, the app continues to run on the background, causing the battery of the phone to die quickly.
- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"%@",[[HistoryCache sharedCache] objectForKey:@"Brands/Brands.png"]);
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"%@",[[HistoryCache sharedCache] objectForKey:@"Brands/Brands.png"]);
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}
- (void)applicationSignificantTimeChange:(UIApplication *)application{

}

//One has approximately 5 seconds to do stuff in here
//Might not be called if app is suspended in the background
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"terminated");
    //Want to clean the Library/Caches Folder on disk
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDirectory = [paths objectAtIndex:0];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //This will remove all files in the Caches Directory
    [fileManager removeItemAtPath:cachesDirectory error:nil];
    for (NSString *file in [fileManager contentsOfDirectoryAtPath:cachesDirectory error:nil]) {
        BOOL success = [fileManager removeItemAtPath:[[cachesDirectory stringByAppendingString:@"/"] stringByAppendingString:file] error:nil];
      
        if (!success) {
            // it failed.
        }
    }
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    [[Branch getInstance] handleDeepLink:url];
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(uint64_t)getFreeDiskspace {
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        NSLog(@"Memory Capacity of %f MB with %f MB Free memory available.", (((totalSpace/1024ll)/1024ll))*1.04858, (((totalFreeSpace/1024ll)/1024ll))*1.04858);
    } else {
        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalFreeSpace;
}

@end

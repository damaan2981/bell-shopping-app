//
//  AppDelegate.h
//  App
//
//  Created by Internet Wu on 29/08/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

//the AppDelegate watches what is going on at the highest level of the application and acts accordingly

#import <UIKit/UIKit.h>
#import <AWSCore/AWSCore.h>
#import "User.h"
#import "AWSIdentityManager.h"
#import "AWSMobileClient.h"
#import "DeveloperAuthenticatedIdentityProvider.h"
#import "HistoryCache.h"
#import "HistoryMemory.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end


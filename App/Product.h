//
//  Product.h
//  App
//
//  Created by Internet Wu on 05/09/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject
@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *categories_ids;
@end

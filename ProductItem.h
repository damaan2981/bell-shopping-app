//
//  ProductItem.h
//  App
//
//  Created by Internet Wu on 24/10/15.
//  Copyright © 2015 Bell Platform. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ProductItem : NSObject
@property (nonatomic) int product_id;
@property (nonatomic) int product_brand_id;
@property (nonatomic, strong) NSString *product_name;
@property (nonatomic, strong) NSString *product_description;
@property (nonatomic) int product_available_quantity;
@property (nonatomic, strong) UIImage *picture;
@property (nonatomic) float price;
@property (nonatomic, strong) NSURL *URL; // To store the URL of the image
@property (nonatomic, readonly) BOOL hasImage; // Return YES if image is downloaded.
@property (nonatomic, getter = isFailed) BOOL failed; // Return Yes if image failed to be downloaded

- (void)setProduct_id:(int)id;
- (void)setProduct_brand_id:(int)brand_id;
- (void)setProduct_name:(NSString *)name;
- (void)setProduct_description:(NSString *)description;
- (void)setProduct_available_quantity:(int)quantity;
- (void)setPicture:(UIImage *)photo;
- (void)setPrice:(float)markup;
- (void)setProductURL:(NSURL *)url;
- (int)getProduct_id;
- (int)getProduct_brand_id;
- (NSString *)getProduct_name;
- (NSString *)getProduct_description;
- (int)getProduct_available_quantity;
- (UIImage *)getPicture;
- (float)getPrice;
- (NSURL *)getProductURL;
@end

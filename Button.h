//
//  Button.h
//  App
//
//  Created by Internet Wu on 13/09/15.
//  Copyright (c) 2015 Bell Platform. All rights reserved.
//

#import "AWSDynamoDBObjectMapper.h"
#import <UIKit/UIKit.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import <Foundation/Foundation.h>

@interface Button : AWSDynamoDBObjectModel
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *page;
@property (nonatomic, strong) NSNumber *x;
@property (nonatomic, strong) NSNumber *y;
@property (nonatomic, strong) NSNumber *height;
@property (nonatomic, strong) NSNumber *width;
@property (nonatomic, strong) NSString *brand;
@property (nonatomic, strong) NSString *type;

@end
